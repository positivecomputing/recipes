Fitbit Client is a proof-of-concept application for retrieving data from the FitBit API. 

###Configuration

You will need to create a `fitbit.properties` file in `src/main/resources/`, and specify the following properties:

* **fitbit.siteUrl** The url for the main fitbit site
* **fitbit.apiUrl** The url for the fitbit api
* **fitbit.clientId** The id of the FitBit application used to identify and authenticate users
* **fitbit.clientSecret** The oauth password of the FitBit application.
    
###Running

From the project root, execute `mvn tomcat7:run`

You should then be able to see the fitbit client interface in your browser at [localhost:8080](http://localhost:8080). 

You'll need to authenticate with Fitbit to do much of anything, so click the link to do so.

After that, you can interact with the api endpoints:

* [/me](localhost:8080/me) retrieves the user's fitbit profile
* [/sleep](localhost:8080/sleep) retrieves the user's sleep data
* [/activities](localhost:8080/activities) retrieves the user's activity data
* [/steps](localhost:8080/steps) retrieves the user's step data

