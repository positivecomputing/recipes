package org.poscomp.sleeptrack.util;

import com.fitbit.api.client.*;
import com.fitbit.api.client.service.FitbitAPIClientService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Created by dmilne on 8/07/2014.
 */
@Component
@Scope(value="singleton")
@PropertySource("classpath:fitbit.properties")
public class FitbitUtils implements InitializingBean {


    private FitbitAPIEntityCache entityCache;
    private FitbitApiCredentialsCache credentialsCache;
    private FitbitApiSubscriptionStorage subscriptionStore;

    private FitbitAPIClientService<FitbitApiClientAgent> apiClientService;


    @Autowired
    private Environment env;

    private String siteUrl;
    private String apiUrl;

    private String clientId;
    private String clientSecret;

    @Override
    public void afterPropertiesSet() throws Exception {

        siteUrl = env.getProperty("fitbit.siteUrl") ;
        apiUrl = env.getProperty("fitbit.apiUrl") ;

        clientId = env.getProperty("fitbit.clientId") ;
        clientSecret = env.getProperty("fitbit.clientSecret") ;

        credentialsCache = new FitbitApiCredentialsCacheMapImpl();
        entityCache = new FitbitApiEntityCacheMapImpl();
        subscriptionStore = new FitbitApiSubscriptionStorageInMemoryImpl();

        apiClientService = new FitbitAPIClientService<FitbitApiClientAgent>(
                new FitbitApiClientAgent(apiUrl, siteUrl, credentialsCache),
                clientId,
                clientSecret,
                credentialsCache,
                entityCache,
                subscriptionStore
        );
    }

    public FitbitAPIClientService<FitbitApiClientAgent> getService() {
        return apiClientService ;
    }
}
