package org.poscomp.sleeptrack.controller;

import com.fitbit.api.FitbitAPIException;
import com.fitbit.api.client.LocalUserDetail;
import com.fitbit.api.common.model.user.UserInfo;
import com.fitbit.api.model.APIResourceCredentials;
import com.mangofactory.swagger.annotations.ApiIgnore;
import org.poscomp.sleeptrack.util.FitbitUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

/**
 * Created by dmilne on 8/07/2014.
 */
@Controller
public class AuthenticationController {

    private static Logger logger = LoggerFactory.getLogger(AuthenticationController.class) ;


    @Autowired
    private FitbitUtils fitbit ;


    @ApiIgnore
    @RequestMapping(value="/login", method = RequestMethod.GET)
    private String login(HttpServletRequest request) throws FitbitAPIException {

        return "redirect:" + fitbit.getService().getResourceOwnerAuthorizationURL(new LocalUserDetail("-"), getServerUrl(request) + "/callback");
    }

    @ApiIgnore
    @RequestMapping(value="/callback", method = RequestMethod.GET)
    private String handleCallback(
            @RequestParam String oauth_token,
            @RequestParam String oauth_verifier
    )  throws FitbitAPIException {

        logger.info("token: " + oauth_token) ;
        logger.info("verifier: " + oauth_verifier) ;

        APIResourceCredentials resourceCredentials = fitbit.getService().getResourceCredentialsByTempToken(oauth_token);

        resourceCredentials.setTempTokenVerifier(oauth_verifier);

        fitbit.getService().getTokenCredentials(new LocalUserDetail("-"));

        return "redirect:./me" ;
    }

    @RequestMapping(value="/me", method = RequestMethod.GET)
    private @ResponseBody
    UserInfo geMe() throws FitbitAPIException, ParseException {

        return fitbit.getService().getClient().getUserInfo(new LocalUserDetail("-")) ;


    }


    public String getServerUrl(HttpServletRequest req) {

        String scheme = req.getScheme();
        String serverName = req.getServerName();
        int serverPort = req.getServerPort();
        String contextPath = req.getContextPath();

        StringBuilder url = new StringBuilder() ;

        url
                .append(scheme)
                .append("://")
                .append(serverName);

        if ((serverPort != 80) && (serverPort != 443)) {
            url
                    .append(":")
                    .append(serverPort);
        }

        url.append(contextPath) ;

        return url.toString() ;
    }
}
