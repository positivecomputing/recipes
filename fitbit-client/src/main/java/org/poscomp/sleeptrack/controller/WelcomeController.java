package org.poscomp.sleeptrack.controller;

import com.mangofactory.swagger.annotations.ApiIgnore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by dmilne on 17/07/2014.
 */
@Controller
public class WelcomeController {

    @ApiIgnore
    @RequestMapping(value = "/", method= RequestMethod.GET)
    public String home() {
        return "index.html";
    }
}
