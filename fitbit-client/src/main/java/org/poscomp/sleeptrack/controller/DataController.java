package org.poscomp.sleeptrack.controller;

import com.fitbit.api.FitbitAPIException;
import com.fitbit.api.client.LocalUserDetail;
import com.fitbit.api.common.model.activities.Activities;
import com.fitbit.api.common.model.sleep.Sleep;
import com.fitbit.api.common.model.timeseries.Data;
import com.fitbit.api.common.model.timeseries.TimePeriod;
import com.fitbit.api.common.model.timeseries.TimeSeriesResourceType;
import com.fitbit.api.model.FitbitUser;
import org.joda.time.LocalDate;
import org.poscomp.sleeptrack.util.FitbitUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by dmilne on 8/07/2014.
 */
@Controller
public class DataController {

    @Autowired
    private FitbitUtils fitbit ;

    private DateFormat format = new SimpleDateFormat("yyyyMMdd") ;


    @RequestMapping(value="/activities", method = RequestMethod.GET)
    private @ResponseBody
    Activities getActivities(
            @RequestParam(required = false) String date
    ) throws FitbitAPIException, ParseException {

        LocalDate localDate ;
        if (date == null)
            localDate = new LocalDate() ;
        else
            localDate = new LocalDate(format.parse(date)) ;

        return fitbit.getService().getActivities(new LocalUserDetail("-"), localDate) ;
    }

    @RequestMapping(value="/sleep", method = RequestMethod.GET)
    private @ResponseBody
    Sleep getSleep(
            @RequestParam(required = false) String date

    ) throws FitbitAPIException, ParseException {

        LocalDate localDate ;
        if (date == null)
            localDate = new LocalDate() ;
        else
            localDate = new LocalDate(format.parse(date)) ;

        return fitbit.getService().getClient().getSleep(new LocalUserDetail("-"), FitbitUser.CURRENT_AUTHORIZED_USER, localDate) ;
    }

    @RequestMapping(value="/steps", method = RequestMethod.GET)
    private @ResponseBody
    List<Data> getSteps(
            @RequestParam(required = false) String date

    ) throws FitbitAPIException, ParseException {

        LocalDate localDate ;
        if (date == null)
            localDate = new LocalDate() ;
        else
            localDate = new LocalDate(format.parse(date)) ;

        return fitbit.getService().getClient().getTimeSeries(new LocalUserDetail("-"), FitbitUser.CURRENT_AUTHORIZED_USER, TimeSeriesResourceType.STEPS, localDate, TimePeriod.SEVEN_DAYS) ;

    }
}
