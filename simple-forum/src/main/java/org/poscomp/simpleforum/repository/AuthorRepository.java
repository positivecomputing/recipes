package org.poscomp.simpleforum.repository;

import org.bson.types.ObjectId;
import org.poscomp.simpleforum.model.MAuthor;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by dmilne on 10/07/2014.
 */
public interface AuthorRepository extends MongoRepository<MAuthor, ObjectId> {

    MAuthor findByApiKey(String apiKey);

    MAuthor findByScreenName(String screenName) ;


}
