package org.poscomp.simpleforum.view;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.poscomp.simpleforum.model.MAuthor;

/**
 * Created by dmilne on 14/07/2014.
 */
@ApiModel(description = "A user")
public class Author {

    private String id ;
    private String screenName ;

    protected Author() {

    }

    public Author(MAuthor author) {
        this.id = author.getId().toString() ;
        this.screenName = author.getScreenName() ;
    }

    @ApiModelProperty(value="an automatically-assigned unique id")
    public String getId() { return id; }

    @ApiModelProperty(value="an alias that is unique, but subject to change")
    public String getScreenName() { return screenName; }
}
