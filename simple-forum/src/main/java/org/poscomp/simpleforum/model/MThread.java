package org.poscomp.simpleforum.model;

import com.wordnik.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

/**
 * Created by dmilne on 10/07/2014.
 */
public class MThread {

    public static final int PAGE_SIZE = 20 ;

    @Id
    private String id ;

    private String title ;

    private Date createdAt ;

    @Indexed
    private Date updatedAt ;

    @Indexed
    private ObjectId creatorId ;

    private int postCount ;

    private int viewCount ;

    private MThread() {

    }

    public MThread(String id, String title, MAuthor creator) {
        this.id = id ;
        this.title = title ;
        this.creatorId = creator.getId() ;

        postCount = 0 ;
        viewCount = 0 ;

        createdAt = new Date() ;
        updatedAt = new Date() ;
    }

    public MThread(String id, String title, MAuthor creator, Date createdAt, Date updatedAt, int postCount, int viewCount) {

        this.id = id ;
        this.title = title ;
        this.creatorId = creator.getId() ;

        this.createdAt = createdAt ;
        this.updatedAt = updatedAt ;

        this.postCount = postCount ;
        this.viewCount = viewCount ;
    }

    public MThread(MThread thread) {

        this.id = thread.getId() ;
        this.title = thread.getTitle() ;
        this.creatorId = thread.getCreatorId() ;

        this.createdAt = thread.getCreatedAt() ;
        this.updatedAt = thread.getUpdatedAt() ;

        this.postCount = thread.getPostCount() ;
        this.viewCount = thread.getViewCount() ;
    }

    @ApiModelProperty(value="a unique, automatically defined id")
    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public ObjectId getCreatorId() {
        return creatorId;
    }

    public int getPostCount() {
        return postCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void handleNewPost() {
        this.updatedAt = new Date() ;
        postCount ++ ;
    }

    public void handleView() {
        viewCount ++ ;
    }
}
