package org.poscomp.simpleforum.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

/**
 * Created by dmilne on 10/07/2014.
 */
public class MPost {

    public static final int PAGE_SIZE = 20 ;

    @Id
    private ObjectId id ;

    @Indexed
    private Date createdAt ;

    private Date lastEditedAt ;

    @Indexed
    private ObjectId creatorId ;

    @Indexed
    private String threadId ;

    private String content ;

    private MPost() {

    }

    public MPost(MAuthor creator, MThread thread, String content) {

        this.creatorId = creator.getId() ;
        this.createdAt = new Date() ;

        this.threadId = thread.getId() ;

        this.content = content ;
    }

    public MPost(MPost post) {

        this.id = post.getId() ;
        this.createdAt = post.getCreatedAt() ;
        this.lastEditedAt = post.getLastEditedAt() ;

        this.creatorId = post.getCreatorId() ;

        this.threadId = post.getThreadId() ;

        this.content = post.getContent() ;
    }

    public ObjectId getId() {
        return id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getLastEditedAt() {
        return lastEditedAt;
    }

    public ObjectId getCreatorId() {
        return creatorId;
    }

    public String getThreadId() {
        return threadId;
    }

    public String getContent() {
        return content ;
    }

    public void setContent(String content) {
        this.content = content ;
        this.lastEditedAt = new Date() ;
    }
}
