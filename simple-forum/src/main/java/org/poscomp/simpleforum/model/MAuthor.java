package org.poscomp.simpleforum.model;

import org.bson.types.ObjectId;
import org.poscomp.simpleforum.util.RandomKeyGenerator;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * Created by dmilne on 10/07/2014.
 */
public class MAuthor {

    private static RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator() ;

    @Id
    private ObjectId id ;

    @Indexed
    private String screenName ;

    @Indexed
    private String apiKey ;

    public MAuthor() {

        resetApiKey() ;
    }

    public MAuthor(MAuthor author) {

        this.id = author.getId() ;
        this.screenName = author.getScreenName() ;
        this.apiKey = author.getApiKey() ;
    }


    public ObjectId getId() {
        return id;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName ;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void resetApiKey() {
        apiKey = randomKeyGenerator.generate() ;
    }
}
