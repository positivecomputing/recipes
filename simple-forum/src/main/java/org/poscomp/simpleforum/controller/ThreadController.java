package org.poscomp.simpleforum.controller;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.poscomp.simpleforum.error.BadRequest;
import org.poscomp.simpleforum.error.Forbidden;
import org.poscomp.simpleforum.error.NotFound;
import org.poscomp.simpleforum.error.Unauthorized;
import org.poscomp.simpleforum.model.MAuthor;
import org.poscomp.simpleforum.model.MThread;
import org.poscomp.simpleforum.view.Thread;
import org.poscomp.simpleforum.repository.PostRepository;
import org.poscomp.simpleforum.repository.ThreadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by dmilne on 10/07/2014.
 */
@Api(value = "threads", position = 2)
@Controller
public class ThreadController extends ControllerBase {

    @Autowired
    private ThreadRepository threadRepo ;

    @Autowired
    private PostRepository postRepo ;


    @ApiOperation(
            value = "List the most recently edited threads",
            notes = "Returns a list of threads, in descending order of when they were last updated."
    )
    @RequestMapping(value="/threads", method= RequestMethod.GET)
    public @ResponseBody Collection<Thread> getThreads(


    ) {

        Map<ObjectId,MAuthor> cachedAuthors = new HashMap<ObjectId,MAuthor>() ;

        Collection<Thread> threads = new ArrayList<Thread>() ;

        for (MThread thread:threadRepo.findRecentlyUpdated()) {

            MAuthor author = cachedAuthors.get(thread.getCreatorId()) ;

            if (author == null) {
                author = authorRepo.findOne(thread.getCreatorId()) ;
                cachedAuthors.put(thread.getCreatorId(), author) ;
            }

            threads.add(new Thread(thread, author)) ;
        }

        return threads ;
    }


    @ApiOperation(
            value = "Returns a single thread",
            notes = "Returns a single thread, identified by the given id"
    )
    @RequestMapping(value="/threads/{threadId}", method=RequestMethod.GET)
    public @ResponseBody Thread getThread(

            @ApiParam(value = "THe id of the thread to return")
            @PathVariable String threadId

    ) throws NotFound {

        MThread thread = threadRepo.findOne(threadId) ;

        if (thread == null)
            throw new NotFound(threadId + " does not exist") ;

        return new Thread(thread, authorRepo.findOne(thread.getCreatorId())) ;
    }


    @ApiOperation(
            value = "Deletes a single thread",
            notes = "Deletes a single thread, and any posts within it"
    )
    @RequestMapping(value="/threads/{threadId}", method=RequestMethod.DELETE)
    public void deleteThread(

            @ApiParam(value = "THe id of the thread to delete")
            @PathVariable String threadId,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized, Forbidden, NotFound {

        MAuthor caller = getCaller(session, apikey) ;

        if (caller == null)
            throw new Unauthorized("You must be signed in to remove threads") ;

        MThread thread = threadRepo.findOne(threadId) ;

        if (thread == null)
            throw new NotFound(threadId + " does not exist") ;

        if (!thread.getCreatorId().equals(caller.getId()))
           throw new Forbidden("Only the creator of a thread can delete it") ;

        threadRepo.remove(thread);

        postRepo.deleteByThreadId(threadId) ;

    }

    @ApiOperation(
            value = "Either creates or edits a thread",
            notes = "Either creates a new thread or edits an existing one\n\n" +
                    "* If you are creating a new thread, you should only specify **title**\n" +
                    "* If you are editing an existing thread, you should only specify **id** and **title**\n" +
                    "All other fields are defined automatically."
    )
    @RequestMapping(value="/threads", method=RequestMethod.POST)
    public @ResponseBody Thread postThread(

            @ApiParam(value = "A json object representing the created or edited thread", required = true)
            @RequestBody Thread thread,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session

    ) throws Unauthorized, Forbidden, BadRequest {

        MAuthor caller = getCaller(session, apikey) ;

        //if this is an anonymous user, create a placeholder author.
        if (caller == null) {
            caller = new MAuthor() ;
            authorRepo.save(caller) ;
            setCaller(caller, session) ;
        }

        if (thread.getTitle() == null)
            throw new BadRequest("You must specify a thread title") ;

        String threadId = thread.getId() ;

        if (threadId == null)
            threadId = threadRepo.getAvailableThreadId(thread.getTitle()) ;

        MThread existingThread = threadRepo.findOne(threadId) ;

        MThread newThread ;
        if (existingThread == null) {
            newThread = new MThread(threadId, thread.getTitle(), caller) ;
        } else {

            if (!existingThread.getCreatorId().equals(caller.getId()))
                throw new Forbidden("You cannot edit someone else's thread") ;

            newThread = new MThread(
                    threadId,
                    thread.getTitle(),
                    caller,
                    existingThread.getCreatedAt(),
                    existingThread.getUpdatedAt(),
                    existingThread.getPostCount(),
                    existingThread.getViewCount()
            ) ;
        }

        threadRepo.save(newThread);

        return new Thread(newThread, caller) ;
    }



}
