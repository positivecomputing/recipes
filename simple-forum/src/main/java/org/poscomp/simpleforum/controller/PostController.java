package org.poscomp.simpleforum.controller;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.poscomp.simpleforum.error.BadRequest;
import org.poscomp.simpleforum.error.Forbidden;
import org.poscomp.simpleforum.error.NotFound;
import org.poscomp.simpleforum.error.Unauthorized;
import org.poscomp.simpleforum.model.MAuthor;
import org.poscomp.simpleforum.model.MPost;
import org.poscomp.simpleforum.model.MThread;
import org.poscomp.simpleforum.repository.PostRepository;
import org.poscomp.simpleforum.repository.ThreadRepository;
import org.poscomp.simpleforum.view.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dmilne on 10/07/2014.
 */

@Api(value = "posts", position = 3)
@Controller
public class PostController extends ControllerBase {

    @Autowired
    private ThreadRepository threadRepo ;

    @Autowired
    private PostRepository postRepo ;


    @ApiOperation(
            value = "Lists posts within a thread",
            notes = "Returns a list of posts for a single thread, in ascending order of when they were created.\n\n" +
                    "This will only return at most " + MPost.PAGE_SIZE + " posts" +
                    ", so use **page** to get more if necessary"
    )
    @RequestMapping(value="/threads/{threadId}/posts", method= RequestMethod.GET)
    public @ResponseBody Collection<Post> getPosts(

            @ApiParam(value = "The id of the thread this post belongs to", required = true)
            @PathVariable String threadId,

            @ApiParam(value = "An optional filter to page through threads with more than " + MPost.PAGE_SIZE + " posts")
            @RequestParam(defaultValue="0") int page

    ) throws NotFound {

        MThread thread = threadRepo.findOne(threadId) ;

        if (thread == null)
            throw new NotFound(threadId + " does not exist") ;

        thread.handleView();
        threadRepo.save(thread) ;

        Map<ObjectId,MAuthor> cachedAuthors = new HashMap<ObjectId,MAuthor>() ;

        Collection<Post> posts = new ArrayList<Post>() ;

        PageRequest pageRequest = new PageRequest(page, MPost.PAGE_SIZE, new Sort(Sort.Direction.ASC, "createdAt")) ;

        for (MPost post:postRepo.findByThreadId(threadId, pageRequest)) {

            MAuthor author = cachedAuthors.get(post.getCreatorId()) ;

            if (author == null) {
                author = authorRepo.findOne(thread.getCreatorId()) ;
                cachedAuthors.put(thread.getCreatorId(), author) ;
            }
            posts.add(new Post(post, author)) ;
        }

        return posts ;
    }

    @ApiOperation(
            value = "Returns a single post",
            notes = "Returns a single post, identified by thread and post id"
    )
    @RequestMapping(value="/threads/{threadId}/posts/{postId}", method=RequestMethod.GET)
    public @ResponseBody Post getPost(

            @ApiParam(value = "The id of the thread this post belongs to", required = true)
            @PathVariable String threadId,

            @ApiParam(value = "The id of the post to retrieve", required = true)
            @PathVariable String postId

    ) throws NotFound, BadRequest {

        if (!ObjectId.isValid(postId))
            throw new BadRequest(postId + " is not a valid post id") ;

        ObjectId pId = new ObjectId(postId) ;

        MPost post = postRepo.findOne(pId) ;

        if (post == null)
            throw new NotFound(postId + "does not exist") ;

        if (!post.getThreadId().equals(threadId))
            throw new BadRequest("post does not belong to " + threadId) ;

        return new Post(post, authorRepo.findOne(post.getCreatorId())) ;
    }

    @ApiOperation(
            value = "Either creates or edits a post",
            notes = "Either creates a new post or edits an existing one\n\n" +
                    "* If you are creating a new post, you should only specify **content**\n" +
                    "* If you are editing an existing post, you should only specify **id** and **content**\n" +
                    "All other fields are defined automatically."
    )
    @RequestMapping(value="/threads/{threadId}/posts", method=RequestMethod.POST)
    public @ResponseBody Post postPost(

            @ApiParam(value = "The id of the thread this post belongs to", required = true)
            @PathVariable String threadId,

            @ApiParam(value = "A json object representing the created or edited post", required = true)
            @RequestBody Post post,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session

    ) throws Unauthorized, Forbidden, BadRequest, NotFound {

        MAuthor caller = getCaller(session, apikey) ;

        //if this is an anonymous user, create a placeholder author.
        if (caller == null) {
            caller = new MAuthor() ;
            authorRepo.save(caller) ;
            setCaller(caller, session) ;
        }

        MThread thread = threadRepo.findOne(threadId) ;

        if (thread == null)
            throw new NotFound(threadId + " does not exist") ;

        if (post.getId() == null)  {

            MPost newPost = new MPost(caller, thread, post.getContent()) ;

            postRepo.save(newPost) ;

            thread.handleNewPost() ;
            threadRepo.save(thread) ;

            return new Post(newPost, caller) ;

        } else {

            MPost existingPost = postRepo.findOne(new ObjectId(post.getId())) ;

            if (existingPost == null)
                throw new NotFound("The post you are trying to edit does not exist. Do not specify ids for new posts") ;

            if (!existingPost.getCreatorId().equals(caller.getId()))
                throw new Forbidden("You cannot edit someone else's post") ;

            if (!existingPost.getThreadId().equals(threadId))
                throw new BadRequest("post does not belong to " + threadId) ;

            existingPost.setContent(post.getContent());
            postRepo.save(existingPost) ;

            return new Post(existingPost, caller) ;
        }

    }


}
