package org.poscomp.simpleforum.controller;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.poscomp.simpleforum.error.BadRequest;
import org.poscomp.simpleforum.error.Forbidden;
import org.poscomp.simpleforum.error.NotFound;
import org.poscomp.simpleforum.error.Unauthorized;
import org.poscomp.simpleforum.model.MAuthor;
import org.poscomp.simpleforum.view.Author;
import org.poscomp.simpleforum.view.AuthorWithApiKey;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by dmilne on 11/07/2014.
 */
@Api(value = "authors", position = 1)
@Controller
public class AuthorController extends ControllerBase {

    @ApiOperation(
            value = "Returns a single user",
            notes = "Returns a single user, identified either by screen name or id"
    )
    @RequestMapping(value="/authors/{screenNameOrId}", method= RequestMethod.GET)
    public @ResponseBody Author getAuthor(

            @ApiParam(value = "The id or screen name of the account to retrieve")
            @PathVariable String screenNameOrId

    ) throws NotFound, BadRequest {

        MAuthor author ;

        if (ObjectId.isValid(screenNameOrId))
            author = authorRepo.findOne(new ObjectId(screenNameOrId)) ;
        else
            author = authorRepo.findByScreenName(screenNameOrId) ;

        if (author == null)
            throw new NotFound(screenNameOrId + "does not exist") ;

        return new Author(author) ;
    }

    @ApiOperation(
            value = "Returns the current user",
            notes = "Returns the currently signed in user, if they have manually created an account or done something to " +
                    "have an anonymous account created for them (i.e. creating a thead or post). Otherwise, an Unauthorized error is thrown"
    )
    @RequestMapping(value="/authors/me", method=RequestMethod.GET)
    public @ResponseBody AuthorWithApiKey getMe(

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized {

        MAuthor caller = getCaller(session, apikey) ;

        if (caller == null)
            throw new Unauthorized("You need to create something to get an author account") ;

        return new AuthorWithApiKey(caller) ;
    }


    @ApiOperation(
            value = "Either creates or edits the account of the current user",
            notes = "Either edits the current user's account, or creates it if they do not yet have an account.\n\n" +
                    "The account details you post should only define the screen name; all other fields are ignored. " +
                    "You cannot edit your id or api key using this method, as these are assigned automatically"
    )
    @RequestMapping(value="/authors", method=RequestMethod.POST)
    public @ResponseBody AuthorWithApiKey postMe(

            @ApiParam(value = "The details of the new or edited account")
            @RequestBody Author caller,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized, Forbidden, BadRequest {

        MAuthor existingCaller = getCaller(session, apikey) ;

        MAuthor screenNameCollision = null ;
        if (caller.getScreenName() != null)
            screenNameCollision = authorRepo.findByScreenName(caller.getScreenName()) ;


        if (existingCaller == null) {

            if (screenNameCollision != null)
                throw new BadRequest("This screen name is already used") ;

            MAuthor newCaller = new MAuthor() ;
            newCaller.setScreenName(caller.getScreenName()) ;
            authorRepo.save(newCaller) ;

            this.setCaller(newCaller, session);

            return new AuthorWithApiKey(newCaller) ;
        } else {

            if (!existingCaller.getId().equals(screenNameCollision.getId()))
                throw new BadRequest("This screen name is already used") ;

            existingCaller.setScreenName(caller.getScreenName()) ;

            authorRepo.save(existingCaller) ;

            return new AuthorWithApiKey(existingCaller) ;
        }
    }






}
