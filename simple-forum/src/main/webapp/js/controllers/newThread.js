function NewThreadCtrl($scope, $location, Restangular, $log) {

	$scope.save = function() {

		Restangular.all('threads').post({title:$scope.threadTitle})
        .then(function(thread) {

        	console.log(thread) ;

        	thread.all('posts').post({content:$scope.postContent})
        	.then(function(post) {

        		console.log(post) ;
                        $location.path("/threads/" + thread.id) ;

        	}) ;
        },
        function(reason) {
        	console.log(reason) ;
        }) ;

	}
}