This web-app project is a basic forum, with threads, posts and authors. It is intended as a 
 toy example that you can crib from, to develop a web application that follows good design 
 principles.

###Dependencies

This app requires:

* An installation of [MongoDB](http://mongodb.org), running locally or available on the network.
* An installation of [Bower](http://bower.io/).

###Configuration

Configuration files are found in `src/main/resources/`

* `mongo.properties` configures MongoDb, the database
    * **mongo.host** The name of the host machine running mongo. Probably leave this as *localhost*
    * **mongo.databaseName** The name of the mongo database used to store all persistent data. This is created automatically on startup, 
                         if it does not exist already.

* `swagger.properties` provides some documentation for the REST API that this app exposes. The fields
  here should be self-explanatory.

###Running

First, navigate to `src/main/webapp` and execute `bower install`

Then navigate back to the project root, and execute `mvn tomcat7:run`

You should then be able to see the forum in your browser at [localhost:8080](http://localhost:8080). 
You can also to interact with the REST api directly using the base path `localhost:8080/api` 
(e.g. [localhost:8080/api/threads](http://localhost:8080/api/threads)).
  
###Design guidelines

This app is designed to cleanly separate data, logic and presentation. 
 
The Java code has absolutely nothing to do with presentation, so it only ever accepts or responds with JSON, 
never with HTML. It's job is to expose a data-only API, which adheres to the 
[REST specification](http://en.wikipedia.org/wiki/REST). 

The presentation layer, in turn, has nothing to do with Java. It can only retrieve and manipulate 
the data by making calls to the API. 

There are a few advantages in designing this way, compared to, say, JSP:
 
* It guarantees that the API is fully featured, so it will be easy for yourself and others to develop new 
  applications that interact with it, or new interfaces (e.g. an iPhone app) to it.
* It reduces the amount of data that needs to be transferred between server and client.
* It defers effort to the client browser, reducing load on the server
* It allows for very rich, flexible interaction. 

####Server Side (Java)

The Java code's job is to provide a nice, clean, well-documented REST API. Once you have the app deployed, this
is available at `localhost:8080/api`

* [Spring MVC](http://spring.io/guides/gs/rest-service) is used to greatly reduce the amount of boilerplate. 
* [MongoDB](http://www.mongodb.org/) is used for object persistence. There is very little code needed to 
  manage this database, thanks to [Spring Data MongoDB](http://projects.spring.io/spring-data-mongodb/).
* The API is documented using [Swagger](https://helloreverb.com/developers/swagger) annotations. This means the 
  documentation of methods, parameters, and models are tightly integrated into the server code, so it will stay 
  in sync as you develop. Once you have the app deployed, this documentation is exposed in json format at 
  `localhost:8080/api/api-docs` (we make it prettier in the presentation layer).
* The code is nicely organized into packages that are general enough to apply to any app that follows the 
  MVC pattern (so you should probably stick to them). 

    * `config` contains everything to do with configuration. These classes replace messy xml 
      files like `application-context.xml`, `web.xml`, etc. 
    * `controller` contains everything to do with logic, and the methods that are exposed as endpoints
      in the REST API. Note how these have been annotated (with `@Api`, `ApiOperation`, `@ApiParam`, etc.) for 
       Swagger documentation. 
    * `error` contains all errors that can be thrown by the controllers. These cover the 
      [usual errors](http://msdn.microsoft.com/en-us/library/azure/dd179357.aspx) people can expect to encounter 
      when calling a REST API. If they get thrown, the API will respond with the proper code, and a nice 
      Json-formatted summary of the error. If a controller throws some other type of exception, it will be 
       returned with the `Internal` (500) error code.
    * `model` contains the basic [POJOs](http://en.wikipedia.org/wiki/Plain_Old_Java_Object) that need 
      to be passed around, manipulated, persisted, etc. Within these classes, any fields that need to be searched 
      or sorted on are annotated with `@Indexed`. It would be fine for these to be sent and recieved by the API, 
      but it is often necessary to add or remove fields first. This is done in the `Views` class, which contains 
      de-normalized versions the model POJOs that are annotated (with `@ApiModel`, `@ApiModelProperty`, etc.) 
      for Swagger documentation. 
    * `repository` contains interfaces to the MongoDb database tables. Note how most of these extend
      [MongoRepository](http://spring.io/guides/gs/accessing-data-mongodb/#_create_simple_queries) so they
      only define methods for operations that fall outside the usual [CRUD](http://en.wikipedia.org/wiki/CRUD). 
      `TheadRepository` provides an example to crib from if you find this too restrictive. 
    * `util` contains a couple of helper classes.     


####Client Side (html, css, javascript)

The client side code puts a pretty face on the API.

* This presentation layer is developed using [AngularJS](https://angularjs.org), so it follows the [MVC architecture 
  pattern](http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller). You can check out this 
  [blog article](http://blog.trifork.com/2014/03/20/web-forms-with-java-angularjs-and-other-approaches/) for
  a good description of why this is a very popular framework, and this [tutorial](https://docs.angularjs.org/tutorial) 
  for getting to grips with it. 
* All external html/css/js libraries are handled by [Bower](http://bower.io/). Just think of this as Maven but for
  Javascript & CSS. It makes it easier to keep track of inter-dependencies, versions, etc, and means this third-party 
  code doesn't need to be stored in the code repository.
* [Bootstrap](http://getbootstrap.com/) and [UI Bootstrap](http://angular-ui.github.io/bootstrap/) are used to 
  provide common ui elements like modals, buttons, etc, minimising the amount of code you need to develop yourself. 
* [FontAwesome](http://fortawesome.github.io/Font-Awesome) provides nice icons. 
  

    