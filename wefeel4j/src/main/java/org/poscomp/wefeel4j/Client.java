package org.poscomp.wefeel4j;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequest;
import org.poscomp.wefeel4j.model.Emotion;
import org.poscomp.wefeel4j.model.Timepoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by dmilne on 15/09/2014.
 */
public class Client {

    public enum Gender {male, female, unknown}
    public enum Granularity {minute, hour, day}
    public enum Count {all, allNoRT, hashtag, hashtagNoRT, endHashtag, endHashtagNoRT} ;


    private static Logger logger = LoggerFactory.getLogger(Client.class) ;

    private String serverUrl = "http://wefeel.csiro.au/api/" ;

    private ObjectMapper jsonMapper ;

    private static final DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy") ;

    private static final TypeReference PrimaryEmotionList = new TypeReference<List<Emotion.Primary>>() { } ;
    private static final TypeReference SecondaryEmotionList = new TypeReference<List<Emotion.Secondary>>() { } ;
    private static final TypeReference RawEmotionList = new TypeReference<List<Emotion.Raw>>() { } ;
    private static final TypeReference TimepointList = new TypeReference<List<Timepoint>>() { } ;


    public Client() {

        initializeJsonMapper(); ;
    }

    public Emotion.Node getEmotionTree() throws UnirestException, IOException, WeFeelException {

        HttpResponse<String> response = Unirest.get(serverUrl + "/emotions/tree")
                .header("accept", "application/json")
                .asString() ;

        logger.info(response.getBody()) ;

        if (isSuccessful(response))
            return jsonMapper.readValue(response.getBody(), Emotion.Node.class) ;
        else
            throw new WeFeelException() ;

    }

    public List<Emotion.Primary> getPrimaryEmotions() throws WeFeelException, UnirestException, IOException {

        HttpResponse<String> response = Unirest.get(serverUrl + "/emotions/primary")
                .header("accept", "application/json")
                .asString() ;

        if (isSuccessful(response))
            return jsonMapper.readValue(response.getBody(), PrimaryEmotionList) ;
        else
            throw new WeFeelException() ;
    }

    public List<Emotion.Secondary> getSecondaryEmotions(String primaryEmotion) throws WeFeelException, UnirestException, IOException {

        HttpResponse<String> response = Unirest.get(serverUrl + "/emotions/primary/" + primaryEmotion + "/secondary")
                .header("accept", "application/json")
                .asString() ;

        if (isSuccessful(response))
            return jsonMapper.readValue(response.getBody(), SecondaryEmotionList) ;
        else
            throw new WeFeelException() ;
    }

    public List<Emotion.Raw> getRawEmotions(String primaryEmotion, String secondaryEmotion) throws WeFeelException, UnirestException, IOException {

        HttpResponse<String> response = Unirest.get(serverUrl + "/emotions/primary/" + primaryEmotion + "/secondary/" + secondaryEmotion + "/raw/")
                .header("accept", "application/json")
                .asString() ;

        if (isSuccessful(response))
            return jsonMapper.readValue(response.getBody(), RawEmotionList) ;
        else
            throw new WeFeelException() ;
    }

    public List<Timepoint> getPrimaryEmotionTimepoints(
            Date start,
            Date end,
            Count count,
            Gender gender,
            Granularity granularity
    ) throws UnirestException, WeFeelException, IOException {

        return getEmotionTimepoints(null, null, start, end, count, gender, granularity) ;
    }

    public List<Timepoint> getSecondaryEmotionTimepoints(
            String primaryEmotion,
            Date start,
            Date end,
            Count count,
            Gender gender,
            Granularity granularity
    ) throws UnirestException, WeFeelException, IOException {

        return getEmotionTimepoints(primaryEmotion, null, start, end, count, gender, granularity) ;
    }

    public List<Timepoint> getRawEmotionTimepoints(
            String primaryEmotion,
            String secondaryEmotion,
            Date start,
            Date end,
            Count count,
            Gender gender,
            Granularity granularity
    ) throws UnirestException, WeFeelException, IOException {

        return getEmotionTimepoints(primaryEmotion, secondaryEmotion, start, end, count, gender, granularity) ;
    }


    private List<Timepoint> getEmotionTimepoints(
            String primaryEmotion,
            String secondaryEmotion,
            Date start,
            Date end,
            Count count,
            Gender gender,
            Granularity granularity
            ) throws UnirestException, WeFeelException, IOException {

        StringBuilder path = new StringBuilder(serverUrl + "/emotions/primary/") ;

        if (primaryEmotion != null)
            path.append(primaryEmotion + "/secondary/") ;

        if (secondaryEmotion != null)
            path.append(secondaryEmotion + "/raw/") ;

        path.append("timepoints") ;


        GetRequest request = Unirest.get(path.toString())
                .header("accept", "application/json") ;

        if (start != null)
            request.field("start", String.valueOf(start.getTime())) ;

        if (end != null)
            request.field("end", String.valueOf(end.getTime())) ;

        if (count != null)
            request.field("count", String.valueOf(count)) ;

        if (gender != null)
            request.field("gender", String.valueOf(gender)) ;

        if (granularity != null)
            request.field("granularity", String.valueOf(granularity)) ;


        HttpResponse<String> response = request.asString() ;

        if (isSuccessful(response))
            return jsonMapper.readValue(response.getBody(), TimepointList) ;
        else
            throw new WeFeelException() ;

    }



    private boolean isSuccessful(HttpResponse<?> response) {

        return response.getCode() >= 200 && response.getCode() < 300 ;
    }


    private void initializeJsonMapper() {

        jsonMapper = new ObjectMapper() ;
        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        jsonMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL) ;
        jsonMapper.setDateFormat(dateFormat) ;
    }
}
