package org.poscomp.wefeel4j.model;

/**
 * Created by dmilne on 15/09/2014.
 */
public class Norms {

    private double valence ;
    private double arousal ;
    private double dominance ;

    public double getValence() {
        return valence;
    }

    public double getArousal() {
        return arousal;
    }

    public double getDominance() {
        return dominance;
    }
}
