package org.poscomp.wefeel4j.model;

import java.util.List;
import java.util.Set;

/**
 * Created by dmilne on 15/09/2014.
 */
public class Emotion {

    private String name ;
    private String path ;

    private Norms norms ;

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public Norms getNorms() {
        return norms;
    }

    public static class Node extends Emotion {

        private List<Node> children ;

        public List<Node> getChildren() {
            return children;
        }
    }

    public static class Primary extends Emotion {

        private Set<String> secondaryEmotions ;

        public Set<String> getSecondaryEmotions() {
            return secondaryEmotions;
        }
    }

    public static class Secondary extends Emotion {

        private String primaryEmotion ;

        private Set<String> rawEmotions ;

        public String getPrimaryEmotion() {
            return primaryEmotion;
        }

        public Set<String> getRawEmotions() {
            return rawEmotions;
        }
    }

    public static class Raw extends Emotion {

        private List<Path> paths ;

        public List<Path> getPaths() {
            return paths;
        }
    }




}
