package org.poscomp.wefeel4j.model;

/**
 * Created by dmilne on 15/09/2014.
 */
public class Path {

        private String primaryEmotion ;
        private String secondaryEmotion ;

        public String getPrimaryEmotion() {
            return primaryEmotion;
        }

        public String getSecondaryEmotion() {
            return secondaryEmotion;
        }

}
