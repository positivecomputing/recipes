package org.poscomp.wefeel4j.model;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

/**
 * Created by dmilne on 15/09/2014.
 */
public class Timepoint {

    private Date start ;

    private Map<String,Integer> counts ;

    public Date getStart() {
        return start;
    }

    public Map<String, Integer> getCounts() {
        return Collections.unmodifiableMap(counts) ;
    }
}
