package org.poscomp;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.Test;
import org.poscomp.wefeel4j.Client;
import org.poscomp.wefeel4j.WeFeelException;
import org.poscomp.wefeel4j.model.Timepoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by dmilne on 15/09/2014.
 */
public class TimepointRetrieval {

    private static DateFormat df = new SimpleDateFormat("dd/MM/yyy HH:mm:ss") ;
    private static Logger logger = LoggerFactory.getLogger(TimepointRetrieval.class) ;

    @Test
    public void testPrimaryEmotionTimepoints() throws ParseException, IOException, UnirestException, WeFeelException {

        Client client = new Client() ;


        List<Timepoint> timepoints = client.getPrimaryEmotionTimepoints(
                df.parse("14/09/2014 00:00:00"),
                df.parse("14/09/2014 23:59:59"),
                null,
                null,
                Client.Granularity.hour
        ) ;

        assertEquals(timepoints.size(), 24) ;

        long joyTotal = 0L ;
        long sadnessTotal = 0L ;

        for (Timepoint t:timepoints) {
            joyTotal = joyTotal + t.getCounts().get("joy") ;
            sadnessTotal = sadnessTotal + t.getCounts().get("sadness") ;
        }

        assertEquals(joyTotal, 21679705) ;
        assertEquals(sadnessTotal, 5759528) ;

    }

    @Test
    public void testSecondaryEmotionTimepoints() throws ParseException, IOException, UnirestException, WeFeelException {

        Client client = new Client() ;


        List<Timepoint> timepoints = client.getSecondaryEmotionTimepoints(
                "joy",
                df.parse("14/09/2014 00:00:00"),
                df.parse("14/09/2014 23:59:59"),
                null,
                null,
                Client.Granularity.hour
        ) ;

        assertEquals(timepoints.size(), 24) ;

        long zestTotal = 0L ;
        long contentmentTotal = 0L ;

        for (Timepoint t:timepoints) {
            zestTotal = zestTotal + t.getCounts().get("joy/zest") ;
            contentmentTotal = contentmentTotal + t.getCounts().get("joy/contentment") ;
        }

        assertEquals(zestTotal, 2434114) ;
        assertEquals(contentmentTotal, 6289772) ;
    }

    @Test
    public void testRawEmotionTimepoints() throws ParseException, IOException, UnirestException, WeFeelException {

        Client client = new Client() ;


        List<Timepoint> timepoints = client.getRawEmotionTimepoints(
                "joy",
                "zest",
                df.parse("14/09/2014 00:00:00"),
                df.parse("14/09/2014 23:59:59"),
                null,
                null,
                Client.Granularity.hour
        ) ;

        assertEquals(timepoints.size(), 24) ;

        long eagerTotal = 0L ;
        long confidentTotal = 0L ;

        for (Timepoint t:timepoints) {
            eagerTotal = eagerTotal + t.getCounts().get("joy/zest/eager") ;
            confidentTotal = confidentTotal + t.getCounts().get("joy/zest/confident") ;
        }

        assertEquals(eagerTotal, 4123) ;
        assertEquals(confidentTotal, 25887) ;
    }
}
