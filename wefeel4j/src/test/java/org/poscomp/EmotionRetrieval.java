package org.poscomp;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.Test;
import org.poscomp.wefeel4j.Client;
import org.poscomp.wefeel4j.WeFeelException;
import org.poscomp.wefeel4j.model.Emotion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by dmilne on 15/09/2014.
 */
public class EmotionRetrieval {

    private static Logger logger = LoggerFactory.getLogger(EmotionRetrieval.class) ;

    @Test
    public void testEmotionTreeRetrieval() throws UnirestException, IOException, WeFeelException {

        Client client = new Client() ;

        Emotion.Node emotionTree = client.getEmotionTree() ;

        Emotion.Node joy = null ;
        Emotion.Node contentment = null ;
        Emotion.Node mellow = null ;


        for (Emotion.Node e:emotionTree.getChildren()) {

            logger.info("Primary Emotion: " + e.getName()) ;
            if (e.getName().equals("joy"))
                joy = e ;
        }

        assertNotNull(joy) ;


        for (Emotion.Node e:joy.getChildren()) {

            logger.info("Secondary Joy Emotion: " + e.getName()) ;

            if (e.getName().equals("contentment"))
                contentment = e ;
        }

        assertNotNull(contentment) ;


        for (Emotion.Node e:contentment.getChildren()) {

            logger.info("Raw Contentment Emotion: " + e.getName()) ;

            if (e.getName().equals("mellow"))
                mellow = e ;
        }

        assertNotNull(mellow) ;

    }

    @Test
    public void testPrimaryEmotionRetrieval() throws IOException, WeFeelException, UnirestException {

        Client client = new Client() ;

        List<Emotion.Primary> primaryEmotions = client.getPrimaryEmotions() ;

        assertTrue(primaryEmotions.size() > 0) ;

        Emotion.Primary sadness = null ;

        for (Emotion.Primary e:primaryEmotions) {
            if (e.getName().equals("sadness"))
                sadness = e ;
        }

        assertNotNull(sadness) ;
    }

    @Test
    public void testSecondaryEmotionRetrieval() throws IOException, WeFeelException, UnirestException {

        Client client = new Client() ;

        List<Emotion.Secondary> secondaryEmotions = client.getSecondaryEmotions("sadness") ;

        assertTrue(secondaryEmotions.size() > 0) ;

        Emotion.Secondary shame = null ;

        for (Emotion.Secondary e:secondaryEmotions) {
            if (e.getName().equals("shame"))
                shame = e ;
        }

        assertNotNull(shame) ;
    }


    @Test
    public void testRawEmotionRetrieval() throws IOException, WeFeelException, UnirestException {

        Client client = new Client() ;

        List<Emotion.Raw> rawEmotions = client.getRawEmotions("sadness", "shame") ;

        assertTrue(rawEmotions.size() > 0) ;

        Emotion.Raw embarrassed = null ;

        for (Emotion.Raw e:rawEmotions) {
            if (e.getName().equals("embarrassed"))
                embarrassed = e ;
        }

        assertNotNull(embarrassed) ;
    }
}
