WeFeel4j is a lightweight Java API that provides programmatic access to [WeFeel](http://wefeel.csiro.au). 

It allows your Java program to communicate with the WeFeel server, to get statistics about the emotional content of Twitter. 


##Limitations

This java client does not yet cover the entire [WeFeel API](http://wefeel.csiro.au/#/api). 

* You cannot filter emotion timepoints by zone.
* You cannot retrieve timepoints by gender or zone.
* You cannot retrieve zones.


##Installation

To use this as a dependency within a Java Maven project, add the following to the `<repositories>` section 
in your project’s `pom.xml` file.

    <repository>
        <id>poscomp-public</id>
        <name>Positive Computing Public Repository</name>
        <url>http://130.56.251.34:8081/nexus/content/groups/public</url>
    </repository>

Then add the following to the `<dependencies>` section

    <dependency>
      <groupId>org.poscomp</groupId>
      <artifactId>wefeel4j</artifactId>
      <version>0.1</version>
    </dependency>

You should then be able to compile the code below.


##Usage

All operations are performed using a **Client**

    Client client = new Client() ;
    
###Emotion Retrieval
    
To retrieve the full tree of emotions:

    Emotion.Node emotionTree = client.getEmotionTree() ;
    
This single node represents the root of the emotion tree. You can access it's children (the primary emotions) using the `emotionTree.getChildren()` method,
and recursively do the same to access the primary and secondary emotions.

If you would prefer to only deal with one level of the tree at a time, you can retrieve a list of primary emotions:

    List<Emotion.Primary> primaryEmotions = client.getPrimaryEmotions() ;
    
There are similar methods for secondary and raw emotions:

    List<Emotion.Secondary> secondaryEmotions = client.getSecondaryEmotions("joy") ;
    List<Emotion.Raw> rawEmotions = client.getRawEmotions("joy","zest") ;

###Timepoint Retrieval

To retrieve a list of timepoints, where each timepoint provides a counts for each primary emotion during a timeframe:

    List<Timepoint> timepoints = client.getPrimaryEmotionTimepoints(
             start,
             end,
             count,
             gender,
             granularity
    ) ;

All of the parameters are optional (they can be set to null):

* **start** specifies the beginning of the first timepoint to retrieve 
* **end** specifies the end of the last timepoint to retrieve
* **count** specifies how counts are calculated (see [API documentation](http://wefeel.csiro.au/#/api) for details)
* **gender** allows you to focus on the emotions of males, females, or accounts of unknown gender
* **granularity** specifies the length of each timepoint (from 5 minutes to an entire day)

There are similar methods to gather timepoints for all secondary emotions within a primary emotion, or all raw emotions within a secondary emotion.

