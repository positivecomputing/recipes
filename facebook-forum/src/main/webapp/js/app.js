
var app = angular.module('app', ['restangular','ui.bootstrap', 'ngRoute', 'ngSanitize']).
	config(['$routeProvider', function($routeProvider) {
  		$routeProvider.
      		when('/', {
                templateUrl: 'partials/threads.html',
                controller: ThreadsCtrl
            }).
            when('/newThread/', {
                templateUrl: 'partials/newThread.html', 
                controller: NewThreadCtrl
            }).
            when('/threads/:threadId', {
                templateUrl: 'partials/thread.html', 
                controller: ThreadCtrl
            }).
            when('/api/', {
                templateUrl: 'partials/api.html', 
                controller: ApiCtrl
            }).
      		otherwise({redirectTo: '/'});
	}]);

app.config(function(RestangularProvider) {

	//all api calls start with this prefix
    RestangularProvider.setBaseUrl('api/');

    

}) ;	


// a simple directive to fire method when user hits enter button
// e.g.: <input type='text' on-enter='someFunction()'></input>

app.directive('onEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.onEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });


app.filter('fromNow', function() {
  return function(date) {
    return moment(date).fromNow();
  }
});

app.filter('markdown', function() {

    var converter = new Showdown.converter();

    return function(markdown) {

        if (!markdown)
            return ;

        return converter.makeHtml(markdown) ;
    }
}) ;