function ApiCtrl($scope, $filter, $routeParams, $http, $modal, $log) {

	$scope.apis = [] ;

	$http.get('/api/api-docs').success(function(data) {

		//console.log(data) ;

		_.each(data.apis, function(api) {

			$scope.apis.push(api) ;

			//console.log(api) ;

			$http.get('/api/api-docs' + api.path).success(function(data) {

				//if (api.description == 'links')
				//	console.log(data) ;

				api.details = data ;
			}) ;
		}) ;

	}) ;

	$scope.getMethodClass = function(verb) {

		switch(verb) {

			case 'GET' : 
				return "label-success" ;
			case 'POST' : 
				return "label-primary" ;
			case 'DELETE' : 
				return "label-danger"
		}
	} ;


	


	$scope.formatPath = function(path) {

  		return path.replace(/\{([^}]*)\}/mg, "<span class='text-muted'>{$1}</span>");
  	}


}


app.directive('apiParameter', function ($sanitize, $modal) {

    return {
      restrict: 'E',
      scope: {
        param: '=',
        paramIndex: '=',
        models: '='
      },
      templateUrl:'apiParameter.html',
      link: function (scope, element, attrs) {

      	scope.getEnumDescription = function(param) {
      		return getEnumDescription(param.enum) ;
      	}
      }
    } 
}) ;


app.directive('apiResponse', function ($sanitize) {

    return {
      restrict: 'E',
      scope: {
        response: '=',
        responseIndex: '=',
        models: '='
      },
      templateUrl:'apiResponse.html',
      link: function (scope, element, attrs) {

      	scope.getResponseCodeClass = function(code) {

			if (code >= 200 && code < 300)
				return "label-success" ;
			else
				return "label-danger" ;
		}
      	
      }
    } 
}) ;



app.directive('apiObjectBadge', function ($sanitize, $modal) {

    return {
      restrict: 'E',
      scope: {
        responseModel: '=',
        propertyType: '=',
        propertyRef: '=',
        models: '='
      },
      templateUrl:'apiObjectBadge.html',
      link: function (scope, element, attrs) {

      	

      	scope.$watch("responseModel", handleChange(), true) ;
      	scope.$watch("propertyType", handleChange(), true) ;
      	scope.$watch("propertyRef", handleChange(), true) ;

      	function handleChange() {

      		//console.log(scope.responseModel + " " + scope.propertyType + " " + scope.propertyRef) ;
      		//console.log(genericsRegex) ;

      		if (!scope.responseModel && !scope.propertyType && !scope.propertyRef)
      			return ;

      		if (scope.responseModel) {

	      		var genericsRegex = /(.+)[«](.+)[»]/g;

	      		var match = genericsRegex.exec(scope.responseModel) ;

	      		if(match) {
	      			scope.objectName = match[2] ;
	      			scope.fullObjectName = match[1] + "<" + match[2] + ">"
	      		} else {
	      			scope.objectName = scope.responseModel ;
	      			scope.fullObjectName = scope.responseModel ;
	      		}

	      		

	      	} else if (scope.propertyType) {

	      		scope.objectName = scope.propertyType ;
	      		scope.fullObjectName = scope.propertyType ;

	      	} else {
	      		scope.objectName = scope.propertyRef ;
	      		scope.fullObjectName = scope.propertyRef ;
	      	}

      		if (scope.models)
      			scope.object = scope.models[scope.objectName] ;

      		//console.log(scope.objectName) ;
      		//console.log(scope.object) ;
      	}

      	scope.showModal = function() {

      		//console.log("blah") ;

		    var modalInstance = $modal.open({
		      templateUrl: 'modalApiObject.html',
		      controller: ModalApiObjectCtrl,
		      size: 'large',
		      resolve: {
		        object: function () {
		          return scope.object;
		        },
		        models: function () {
		        	return scope.models;
		        }
		      }
		    });

		    modalInstance.result.then(function () {
		      //don't need anything from this modal
		    });
  		}


      }
    } 



}) ;

function getEnumDescription(enumArray) {

	if (!enumArray)
		return "" ;

	var str = "" ;

	_.each(enumArray, function(e, index) {

		if (index == 0) {
			str = "" ;
		} else if (index < enumArray.length-1) {
			str = str + ", " ;
		} else {
			str = str + " or " ;
		}

		str = str + "<em>" + e + "</em>" ;
	}) ;

	return "(" + str + ")" ;
}


var ModalApiObjectCtrl = function ($scope, $modalInstance, $sanitize, object, models) {

     	$scope.object = object ;
    	$scope.models = models ;

    	$scope.getEnumDescription = function(property) {
      		return getEnumDescription(property.enum) ;
      	}
} ;