function ThreadCtrl($scope, $filter, $routeParams, Restangular, $modal, $log) {

	$scope.threadId = $routeParams.threadId;

	Restangular.one('threads', $scope.threadId).get()
	.then(function(thread) {

		$scope.thread = thread ;

		updatePosts() ;
	})
	

	function updatePosts() {

		console.log("updating posts") ;

		$scope.posts = null ;

		$scope.thread.all('posts').getList()
    		.then(function(data) {
        		$scope.posts = data ;
        		console.log(data) ;
        	}) ;
	}


	$scope.savePost = function() {

    	if (!$scope.newPostContent)
    		return ;

    	var newPost = {
	  		content:$scope.newPostContent
	  	} ;

	  	$scope.thread.all('posts').post(newPost).then( function(data) {
	  		console.log(data) ;

	  		$scope.newPostContent = undefined ;
	  		$scope.preview = false ;

	  		updatePosts() ;

	  	}, function(error) {

	  		console.log(error) ;
	  		$scope.error = error ;
	  	}) ;
    }
}