function ThreadsCtrl($scope, $filter, $routeParams, Restangular, $modal, $log) {
	
	updateThreads() ;
	updateMe() ;

	function updateThreads() {

		$scope.threads = undefined ;

		Restangular.all('threads').getList()
        .then(function(data) {
        	$scope.threads = data ;
        }) ;
	}

	function updateMe() {

		$scope.me = undefined ;
		$scope.unauthrized = undefined ;

		Restangular.one('authors', 'me').get()
		.then(function(data) {
			$scope.me = data ;
			$scope.unauthorized = false ;
		}, function(error) {
        	console.log(error) ;

        	if (error.status = 401)
        		$scope.unauthorized = true ;
		}) ;

	}

	
}

