function MainCtrl($scope, $filter, $routeParams, Restangular, $modal, $log) {
	
	$scope.error = undefined ;

	$scope.unauthorized = false ;

	Restangular.one('authors', 'me').get()
		.then(function(data) {
			$scope.me = data ;
			$scope.unauthorized = false ;
		}, function(error) {
        	console.log(error) ;

        	if (error.status = 401)
        		$scope.unauthorized = true ;
		}) ;


	$scope.showApiKey = function() {

		var modalInstance = $modal.open({
	      templateUrl: 'modalApiKey.html',
	      controller: DlgApiKeyCtrl,
	      size:'sm',
	      resolve: {
	      	me: function() {
	      		return $scope.me ;
	      	}
	      }
	    });

	    modalInstance.result.then(function (me) {
	    	
	    }) ;
	}

	$scope.editAccount = function() {

		var modalInstance = $modal.open({
	      templateUrl: 'modalEditAccount.html',
	      controller: DlgEditAccountCtrl,
	      size:'sm',
	      resolve: {
	      	me: function() {
	      		return $scope.me ;
	      	}
	      }
	    });

	    modalInstance.result.then(function(me) {
	    	$scope.me = me ;
	    }) ;
	}

}

var DlgApiKeyCtrl = function ($scope, $modalInstance, me) {

	$scope.me = me ;

};

var DlgEditAccountCtrl = function($scope, Restangular, $modalInstance, me) {

	$scope.d = {} ;

	if (me)
		$scope.d.me = {screenName:me.screenName} ;
	else
		$scope.d.me = {screenName:undefined} ;

	$scope.d.save = function() {

		$scope.d.error = undefined ;

		if (!$scope.d.me.screenName || isBlank($scope.d.me.screenName)) {
			$scope.d.error = {data:{message:"You must specify a screen name"}} ;
			return ;
		}

		Restangular.all('authors').post($scope.d.me)
	  	.then(function(result) {
	  		console.log(result)
	  		$modalInstance.close(result); 
	  	}, function(error) {
	  		console.log(error) ;
	  		$scope.d.error = error ;
	  	}) ;

	}
};

