package org.poscomp.simpleforum.repository;

import org.bson.types.ObjectId;
import org.poscomp.simpleforum.model.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by dmilne on 11/07/2014.
 */
public interface PostRepository extends MongoRepository<MPost, ObjectId> {

    List<MPost> findByCreatorId(ObjectId creatorId, PageRequest pageRequest);

    List<MPost> findByThreadId(String threadId, PageRequest pageRequest) ;

    Long deleteByThreadId(String threadId) ;

}
