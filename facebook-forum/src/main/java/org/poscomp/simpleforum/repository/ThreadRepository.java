package org.poscomp.simpleforum.repository;

import org.poscomp.simpleforum.model.MThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by dmilne on 10/07/2014.
 */
@Repository
public class ThreadRepository {

    @Autowired
    private MongoTemplate m ;

    public boolean exists(String id) {

        Query query = new Query(Criteria.where("id").is(id)) ;

        return m.exists(query, MThread.class) ;
    }

    public MThread findOne(String id) {

        return m.findById(id, MThread.class) ;
    }

    public List<MThread> findRecentlyUpdated() {

        Query query = new Query()
                .with(new Sort(Sort.Direction.DESC, "updatedAt")) ;

        return m.find(query, MThread.class) ;
    }

    public void save(MThread thread) {

        m.save(thread) ;
    }

    public void remove(MThread thread) {
        m.remove(thread) ;
    }

    public long getSize() {
        return m.getCollection(m.getCollectionName(MThread.class)).count() ;
    }

    public String getAvailableThreadId(String threadTitle) {

        String normalizedTitle = threadTitle.toLowerCase().trim() ;
        normalizedTitle = normalizedTitle.replaceAll("\\W", "-") ;

        if (!exists(normalizedTitle))
            return normalizedTitle ;

        int i = 0 ;
        do {
            i = i+1 ;
        } while (exists(normalizedTitle + "_" + i)) ;

        return normalizedTitle + "_" + i ;

    }



}
