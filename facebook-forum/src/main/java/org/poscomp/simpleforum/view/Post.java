package org.poscomp.simpleforum.view;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.poscomp.simpleforum.model.MAuthor;
import org.poscomp.simpleforum.model.MPost;

import java.util.Date;

/**
 * Created by dmilne on 14/07/2014.
 */
@ApiModel(description = "A single post within a thread")
public class Post {

    private String id ;
    private Date createdAt ;
    private Date lastEditedAt ;
    private String creatorId ;
    private String creatorScreenName ;
    private String threadId ;
    private String content ;

    private Post() {

    }

    public Post(MPost post, MAuthor creator) {
        this.id = post.getId().toString() ;
        this.createdAt = post.getCreatedAt() ;
        this.lastEditedAt = post.getLastEditedAt() ;
        this.creatorId = post.getCreatorId().toString() ;
        this.creatorScreenName = creator.getScreenName() ;
        this.threadId = post.getThreadId() ;
        this.content = post.getContent() ;
    }

    @ApiModelProperty(value="an automatically assigned unique id")
    public String getId() {
        return id;
    }

    @ApiModelProperty(value="the date this post was created")
    public Date getCreatedAt() { return createdAt ; }

    @ApiModelProperty(value="the date this post was last edited")
    public Date getLastEditedAt() {
        return lastEditedAt;
    }

    @ApiModelProperty(value="the id of this post's creator")
    public String getCreatorId() { return creatorId ; }

    @ApiModelProperty(value="the name of this post's author")
    public String getCreatorScreenName() {
        return creatorScreenName;
    }

    @ApiModelProperty(value="the id of this thread's creator")
    public String getThreadId() {
        return threadId;
    }

    @ApiModelProperty(value="the content of this post, in markdown format")
    public String getContent() {
        return content ;
    }

}
