package org.poscomp.simpleforum.view;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.poscomp.simpleforum.model.MAuthor;
import org.poscomp.simpleforum.model.MThread;

import java.util.Date;

/**
 * Created by dmilne on 14/07/2014.
 */
@ApiModel(description = "A collection of posts")
public class Thread {

    private String id ;
    private String title ;
    private Date createdAt ;
    private Date updatedAt ;
    private String creatorId ;
    private String creatorScreenName ;
    private int postCount ;
    private int viewCount ;

    private Thread() {

    }

    public Thread(MThread thread, MAuthor creator) {
        this.id = thread.getId() ;
        this.title = thread.getTitle() ;
        this.createdAt = thread.getCreatedAt() ;
        this.updatedAt = thread.getUpdatedAt() ;
        this.creatorId = thread.getCreatorId().toString() ;
        this.creatorScreenName = creator.getScreenName() ;
        this.postCount = thread.getPostCount() ;
        this.viewCount = thread.getViewCount() ;
    }

    @ApiModelProperty(value="a unique id, automatically derived from the thread title")
    public String getId() {
        return id;
    }

    @ApiModelProperty(value="a user-defined thread title")
    public String getTitle() { return title; }

    @ApiModelProperty(value="the date this thread was created")
    public Date getCreatedAt() {
        return createdAt;
    }

    @ApiModelProperty(value="the creation date of this thread's most recent post")
    public Date getUpdatedAt() { return updatedAt; }

    @ApiModelProperty(value="the id of this thread's creator")
    public String getCreatorId() { return creatorId; }

    @ApiModelProperty(value="the name of this thread's creator")
    public String getCreatorScreenName() {
        return creatorScreenName;
    }

    @ApiModelProperty(value="the total number of posts in this thread")
    public int getPostCount() { return postCount; }

    @ApiModelProperty(value="the total number of times this thread has been viewed")
    public int getViewCount() { return viewCount; }

}
