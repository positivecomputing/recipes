package org.poscomp.simpleforum.view;

import com.wordnik.swagger.annotations.ApiModelProperty;
import org.poscomp.simpleforum.model.MAuthor;

/**
 * Created by dmilne on 16/09/2014.
 */
public class AuthorWithApiKey extends Author {

    private String apiKey ;

    protected AuthorWithApiKey() {
        super() ;
    }

    public AuthorWithApiKey(MAuthor author) {
        super(author) ;
        this.apiKey = author.getApiKey() ;
    }

    @ApiModelProperty(value="an automatically-assigned api key")
    public String getApiKey() {
        return apiKey;
    }
}
