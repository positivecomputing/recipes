package org.poscomp.simpleforum.controller;

import org.bson.types.ObjectId;
import org.poscomp.simpleforum.error.Unauthorized;
import org.poscomp.simpleforum.model.MAuthor;
import org.poscomp.simpleforum.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;

/**
 * Created by dmilne on 10/07/2014.
 */
public abstract class ControllerBase {

    private static final String USER_ID = "userId" ;

    @Autowired
    protected AuthorRepository authorRepo ;

    public MAuthor getCaller(HttpSession session, String apiKey) throws Unauthorized {

        if (apiKey != null) {
            MAuthor caller = authorRepo.findByApiKey(apiKey) ;

            if (caller == null)
                throw new Unauthorized("Invalid api key") ;

            return caller ;
        } else {

            ObjectId callerId = (ObjectId) session.getAttribute(USER_ID) ;

            if (callerId == null)
                return null ;

            return authorRepo.findOne(callerId) ;
        }
    }

    public void setCaller(MAuthor caller, HttpSession session) throws Unauthorized {

        session.setAttribute(USER_ID, caller.getId());

    }

}
