package org.poscomp;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.poscomp.fbps.util.QueryParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by dmilne on 13/08/2014.
 */
public class QueryParsing {

    private static Logger logger = LoggerFactory.getLogger(QueryParsing.class) ;

    @Test
    public void testUrlParsing() throws URISyntaxException {


        String url = "http://chat.poscomp.org" ;

        URI uri = new URI(url) ;

        assertTrue(uri.isAbsolute()) ;
     }

    @Test
    public void testQueryParsing() {

        String query = "stuff" ;
        List<String> phrases = QueryParser.parse(query) ;
        logger.info("[" + StringUtils.join(phrases, "|") + "]") ;
        assertEquals(phrases.size(), 1) ;
        assertEquals(phrases.get(0), "stuff") ;

        query = "stuff like this" ;
        phrases = QueryParser.parse(query) ;
        logger.info("[" + StringUtils.join(phrases, "|") + "]") ;
        assertEquals(phrases.size(), 3) ;
        assertEquals(phrases.get(0), "stuff") ;
        assertEquals(phrases.get(1), "like") ;
        assertEquals(phrases.get(2), "this") ;

        query = "stuff \"like this\"" ;
        phrases = QueryParser.parse(query) ;
        logger.info("[" + StringUtils.join(phrases, "|") + "]") ;
        assertEquals(phrases.size(), 2) ;
        assertEquals(phrases.get(0), "stuff") ;
        assertEquals(phrases.get(1), "like this") ;

        query = "\"stuff like\" this" ;
        phrases = QueryParser.parse(query) ;
        logger.info("[" + StringUtils.join(phrases, "|") + "]") ;
        assertEquals(phrases.size(), 2) ;
        assertEquals(phrases.get(0), "stuff like") ;
        assertEquals(phrases.get(1), "this") ;

        query = "\"stuff like this\"" ;
        phrases = QueryParser.parse(query) ;
        logger.info("[" + StringUtils.join(phrases, "|") + "]") ;
        assertEquals(phrases.size(), 1) ;
        assertEquals(phrases.get(0), "stuff like this") ;

    }
}
