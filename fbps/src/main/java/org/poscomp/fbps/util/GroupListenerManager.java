package org.poscomp.fbps.util;

import org.poscomp.fbps.model.UserGroup;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmilne on 13/08/2014.
 */
@Component
@Scope(value = "singleton")
public class GroupListenerManager {

    List<GroupListener> listeners = new ArrayList<GroupListener>() ;

    public void add(GroupListener listener) {
        listeners.add(listener) ;
    }

    public void groupCreated(UserGroup group) {
        for (GroupListener listener:listeners)
            listener.groupCreated(group) ;
    }

    public void groupModified(UserGroup group) {
        for (GroupListener listener:listeners)
            listener.groupModified(group) ;
    }

    public void groupDeleted(UserGroup group) {
        for (GroupListener listener:listeners)
            listener.groupDeleted(group) ;
    }


}
