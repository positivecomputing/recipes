package org.poscomp.fbps.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dmilne on 13/08/2014.
 */
public class QueryParser {

    private static Pattern phrasePattern = Pattern.compile("\"([^\"]+)\"", Pattern.DOTALL) ;

    public static List<String> parse(String query) {

        query = query.replaceAll("\\s+", " ") ;

        int lastCopyIndex = 0 ;

        List<String> phrases = new ArrayList<String>() ;

        Matcher m = phrasePattern.matcher(query) ;

        while (m.find()) {

            for (String term:query.substring(lastCopyIndex, m.start()).split(" ")) {

                term = term.trim() ;

                if (term.length() > 0)
                    phrases.add(term) ;
            }

            String phrase = m.group(1).trim() ;
            if (phrase.length() > 0)
                phrases.add(phrase) ;

            lastCopyIndex = m.end() ;
        }

        for (String term:query.substring(lastCopyIndex).split(" ")) {

            term = term.trim() ;

            if (term.length() > 0)
                phrases.add(term) ;
        }

        return phrases ;
    }

}
