package org.poscomp.fbps.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.restfb.DefaultFacebookClient;
import com.restfb.DefaultWebRequestor;
import com.restfb.FacebookClient;
import com.restfb.WebRequestor;
import com.restfb.json.JsonArray;
import com.restfb.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by dmilne on 5/06/2014.
 */
@Component
@Scope(value = "singleton")
@PropertySource("classpath:facebook.properties")
public class FacebookUtils implements InitializingBean {

    private static Logger logger = LoggerFactory.getLogger(FacebookUtils.class) ;

    private static final Pattern appTokenPattern = Pattern.compile(Pattern.quote("access_token=") + "(\\w+)\\|(\\w+)") ;

    private static final String host = "https://graph.facebook.com/v2.0" ;

    @Autowired
    private Environment env;

    private String clientId  ;
    private String clientSecret  ;

    private ObjectMapper jsonMapper ;

    @Override
    public void afterPropertiesSet() throws Exception {

        clientId = env.getProperty("facebook.clientId") ;
        clientSecret = env.getProperty("facebook.clientSecret") ;

        if (clientId == null || clientSecret == null)
            throw new Exception("Could not read facebook id/secret from properties") ;

        jsonMapper = new ObjectMapper() ;
        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    }




    public String getClientId() {
        return clientId ;
    }

    public String getClientSecret() {
        return clientSecret ;
    }

    public FacebookClient getAppClient() {

        FacebookClient.AccessToken token = new DefaultFacebookClient().obtainAppAccessToken(clientId,clientSecret);

        return new DefaultFacebookClient(token.getAccessToken(), clientSecret) ;
    }

    public FacebookClient getUserClient(String token) {
        return new DefaultFacebookClient(token);
    }


    public String getAuthenticationURI(String callback, String stateKey)  {

        StringBuilder sb = new StringBuilder() ;

        sb.append("https://www.facebook.com/dialog/oauth") ;
        sb.append("?client_id=" + clientId);
        //sb.append("&response_type=token");
        sb.append("&redirect_uri=" + callback);
        sb.append("&state=" + stateKey);

        return sb.toString() ;
    }

    public FacebookClient.AccessToken getFacebookUserToken(String code, String redirectUrl) throws IOException {

        WebRequestor wr = new DefaultWebRequestor();
        WebRequestor.Response accessTokenResponse = wr.executeGet(
                "https://graph.facebook.com/oauth/access_token"
                        +"?client_id=" + clientId
                        + "&client_secret=" + clientSecret
                        + "&redirect_uri=" + redirectUrl
                        + "&code=" + code);

        return DefaultFacebookClient.AccessToken.fromQueryString(accessTokenResponse.getBody());
    }

    public Set<String> getAppAdministratorIds()  {

        JsonObject roles = getAppClient().fetchObject("/" + clientId + "/roles", JsonObject.class) ;

        Set<String> adminIds = new HashSet<String>() ;

        JsonArray data = roles.getJsonArray("data") ;

        for (int i=0 ; i<data.length() ; i++) {
            JsonObject r = data.getJsonObject(i) ;

            String role = r.getString("role") ;
            String userId = r.getString("user") ;

            if (role.equals("administrators"))
                adminIds.add(userId) ;
        }

        return adminIds ;
    }


}
