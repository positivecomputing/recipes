package org.poscomp.fbps.util;

import org.poscomp.fbps.model.UserGroup;

/**
 * Created by dmilne on 13/08/2014.
 */
public interface GroupListener {

    public void groupCreated(UserGroup group)  ;

    public void groupModified(UserGroup group)  ;

    public void groupDeleted(UserGroup group)  ;

}
