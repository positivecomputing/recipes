package org.poscomp.fbps.controller;



import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.json.JsonObject;
import org.poscomp.fbps.error.Forbidden;
import org.poscomp.fbps.error.NotFound;
import org.poscomp.fbps.error.Unauthorized;
import org.poscomp.fbps.model.FacebookProfile;
import org.poscomp.fbps.model.GroupMember;
import org.poscomp.fbps.model.User;
import org.poscomp.fbps.model.UserGroup;
import org.poscomp.fbps.repository.GroupMemberRepository;
import org.poscomp.fbps.repository.UserGroupRepository;
import org.poscomp.fbps.repository.UserRepository;
import org.poscomp.fbps.util.FacebookUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by dmilne on 25/06/2014.
 */
@Controller
@PropertySource("classpath:fbps.properties")
public abstract class ControllerBase {

    private static final String USER_ID = "fbpsUserId" ;

    private static Logger logger = LoggerFactory.getLogger(ControllerBase.class) ;

    //private MappingJackson2JsonView jsonView = new MappingJackson2JsonView();

    @Autowired
    private UserRepository userRepo ;

    @Autowired
    private UserGroupRepository groupRepo ;

    @Autowired
    private GroupMemberRepository groupMemberRepo ;

    @Autowired
    private FacebookUtils facebook ;

    @Autowired
    private Environment env ;

    public void setUser(User user, HttpSession session) {
        if (user == null) {
            session.removeAttribute(USER_ID) ;
            //session.removeAttribute(PLAYER_AVATAR_URL) ;
        } else {
            session.setAttribute(USER_ID, user.getUserId()) ;
            //session.setAttribute(PLAYER_AVATAR_URL, player.getAvatarUrl()) ;
        }

        session.setMaxInactiveInterval(-1) ;
    }

    public User getUser(String apiKey, HttpSession session) throws Unauthorized {

        try {
            return getUser(apiKey) ;
        } catch (Unauthorized e) {
            return getUser(session) ;
        }
    }

    private User getUser(HttpSession session) throws Unauthorized {

        Object o = session.getAttribute(USER_ID) ;

        if (o == null)
            throw new Unauthorized("You are not logged in") ;

        String userId = (String)o ;

        User user = userRepo.findOne(userId) ;

        if (user == null)
            throw new Unauthorized("You are not logged in") ;

        return user ;
    }

    private User getUser(String apiKey) throws Unauthorized {

        if (apiKey == null)
            throw new Unauthorized("You must provide a valid api key") ;

        User user = userRepo.findByApiKey(apiKey) ;

        if (user == null)
            throw new Unauthorized("Unknown api key") ;

        return user ;
    }

    public UserGroup getGroup(String groupId) {
        return groupRepo.findById(groupId) ;
    }


    public SortedSet<String> getPermittedGroupIds(User user, GroupMember.Role role) {

        SortedSet<String> groupIds = new TreeSet<String>() ;
        for (GroupMember groupMember:groupMemberRepo.findByUserIdAndMinimumRole(user.getUserId(), role))
            groupIds.add(groupMember.getGroupId()) ;

        for (GroupMember groupMember:groupMemberRepo.findByUserIdAndMinimumRole(GroupMember.ANY, role))
            groupIds.add(groupMember.getGroupId()) ;

        return groupIds ;
    }

    public void checkGroupRole(User user, String groupId, GroupMember.Role role, String forbiddenMessage) throws NotFound, Forbidden {

        GroupMember m = findStrongestMembership(user, groupId) ;

        if (m == null) {
            logger.info("no membership: ") ;
            throw new Forbidden(forbiddenMessage) ;
        }

        if (m.getRole().ordinal() > role.ordinal()) {
            logger.info("insufficient membership: " + m.getUserId() + " " + m.getGroupId() + " " + m.getRole()) ;
            logger.info(m.getRole() + " vs " + role) ;
            throw new Forbidden(forbiddenMessage) ;
        }


        return ;
    }

    /**
     * Returns the membership with the most rights for the given user/group
     *
     * Memberships can be explicitly stated between a user and a group,
     * or they can be implicitly stated between ANY user and a group.
     *
     * Additionally, members with admin roles can be inherited from the root group.
     *
     * @param user
     * @param groupId
     * @return the membership with the strongest role, or null if no relevant memberships exist
     * @throws NotFound if the group does not exist
     */
    public GroupMember findStrongestMembership(User user, String groupId) throws NotFound {

        UserGroup group = groupRepo.findById(groupId) ;
        if (group == null)
            throw new NotFound("Unknown group") ;

        GroupMember explicitMembership = groupMemberRepo.findByGroupIdAndUserId(groupId, user.getUserId()) ;
        GroupMember guestMembership = groupMemberRepo.findByGroupIdAndUserId(groupId, GroupMember.ANY) ;

        SortedSet<GroupMember> sortedMembers = new TreeSet<GroupMember>() ;

        if (explicitMembership != null)
            sortedMembers.add(explicitMembership) ;

        if (guestMembership != null)
            sortedMembers.add(guestMembership) ;

        if (sortedMembers.isEmpty())
            return null ;

        return sortedMembers.first() ;
    }

    public User addOrUpdateUser(String userId, User caller) {

        if (userId.equals(GroupMember.ANY))
            return User.ANYONE ;

        FacebookProfile profile = retrieveProfile(userId, caller.getToken());

        User user = userRepo.findOne(userId);
        if (user == null)
            user = new User(profile);
        else
            user.updateProfile(profile);

        userRepo.save(user);

        return user ;
    }

    public FacebookProfile retrieveProfile(String id, String userToken) {

        FacebookClient client = facebook.getUserClient(userToken) ;

        JsonObject rawProfile = client.fetchObject(
                id,
                JsonObject.class,
                Parameter.with("fields", FacebookProfile.FIELDS)
        ) ;

        return new FacebookProfile(rawProfile) ;
    }




    public String getServerUrl() {


        return env.getProperty("server.url") ;

        /*
        String scheme = req.getScheme();
        String serverName = req.getServerName();
        int serverPort = req.getServerPort();
        String contextPath = req.getContextPath();

        StringBuilder url = new StringBuilder() ;

        url
                .append(scheme)
                .append("://")
                .append(serverName);

        if ((serverPort != 80) && (serverPort != 443)) {
            url
                    .append(":")
                    .append(serverPort);
        }

        url.append(contextPath) ;

        logger.info("Server URL:" + url.toString()) ;

        return url.toString() ;
        */
    }
}

