package org.poscomp.fbps.controller;


import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.poscomp.fbps.error.BadRequest;
import org.poscomp.fbps.error.Forbidden;
import org.poscomp.fbps.error.NotFound;
import org.poscomp.fbps.error.Unauthorized;
import org.poscomp.fbps.model.GroupMember;
import org.poscomp.fbps.model.User;
import org.poscomp.fbps.model.UserGroup;
import org.poscomp.fbps.model.Views;
import org.poscomp.fbps.repository.*;
import org.poscomp.fbps.util.GroupListenerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dmilne on 4/06/2014.
 */
@Api(value = "groups", position = 2)
@Controller
public class GroupController extends ControllerBase {

    private static Logger logger = LoggerFactory.getLogger(GroupController.class) ;

    private static final Pattern validGroupIdPattern = Pattern.compile("[a-z0-9.,-_]+") ;

    @Autowired
    private UserRepository userRepo ;

    @Autowired
    private UserGroupRepository userGroupRepo;

    @Autowired
    private GroupMemberRepository groupMemberRepo ;

    @Autowired
    private GroupMemberRequestRepository requestRepo ;

    @Autowired
    private InvitationRepository invitationRepo ;

    @Autowired
    private GroupListenerManager groupListenerManager ;



    @ApiOperation(
            value = "List all groups",
            notes = "List all groups that match the given query (whether the caller has access to it or not)"
    )
    @RequestMapping(value="/groups", method= RequestMethod.GET)
    public @ResponseBody Collection<Views.UserGroup> getGroups(

            @ApiParam(value = "A simple query filter, so only groups whose titles or descriptions match the query are returned.")
            @RequestParam String query,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized {

        User user = getUser(apikey, session) ;

        List<UserGroup> groups = new ArrayList<UserGroup>() ;

        //look up groups matching query
        for (UserGroup group : userGroupRepo.findByQuery(query))
               groups.add(group);

        return hydrate(groups) ;

    }

    @ApiOperation(
            value = "List all groups that the caller has access to",
            notes = "List all groups that the caller has at-least the given role for. This includes groups for which anyone has the given role."
    )
    @RequestMapping(value="/users/me/groups", method= RequestMethod.GET)
    public @ResponseBody List<Views.UserGroup> getGroupsForUser(

            @ApiParam(value = "A filter, so only groups for which the caller has *at least* this role will be returned", required=false)
            @RequestParam(defaultValue = "guest") GroupMember.Role role,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized {

        User user = getUser(apikey, session) ;

        List<UserGroup> groups = new ArrayList<UserGroup>() ;
        for (String groupId:getPermittedGroupIds(user, role)) {
            UserGroup group = userGroupRepo.findById(groupId) ;

            if (group != null)
                groups.add(group) ;
        }

        return hydrate(groups) ;
    }

    @ApiOperation(
            value = "Checks whether a candidate group id is valid",
            notes = "Checks whether the given group id can be safely used for a new group. This will check that it contains only valid characters, and is not already used."
    )
    @RequestMapping(value="/groups/checkId", method=RequestMethod.GET)
    public @ResponseBody Views.GroupIdCheck checkGroupId(

            @ApiParam(value = "The candidate group id of check", required = true)
            @RequestParam String id,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,
            HttpSession session
    ) throws Unauthorized {

        User user = getUser(apikey, session) ;

        if (!idIsValid(id))
            return new Views.GroupIdCheck(Views.GroupIdCheck.Result.invalid) ;

        UserGroup existingGroup = userGroupRepo.findById(id) ;

        if (existingGroup != null)
            return new Views.GroupIdCheck(Views.GroupIdCheck.Result.taken) ;

        return new Views.GroupIdCheck(Views.GroupIdCheck.Result.ok) ;
    }

    @ApiOperation(
            value = "Returns a single group",
            notes = "Returns a single group, identified by groupId.",
            response = Views.UserGroup.class
    )
    @RequestMapping(value="/groups/{groupId}", method= RequestMethod.GET)
    public @ResponseBody
    Views.UserGroup getGroup(

            @ApiParam(value = "The id of the group to retrieve", required = true)
            @PathVariable String groupId,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized, Forbidden, NotFound {

        User user = getUser(apikey, session) ;

        UserGroup group = userGroupRepo.findById(groupId) ;

        if (group == null)
            throw new NotFound("Group does not exist") ;

        GroupMember groupMember = groupMemberRepo.findByGroupIdAndUserId(group.getId(), user.getUserId()) ;

        User creator = userRepo.findOne(group.getCreatorId()) ;

        return new Views.UserGroup(group, creator) ;
    }

    @ApiOperation(
            value = "Creates a new group, or edits an existing one",
            notes = "Either creates a new group or edits an existing one, depending on whether the group object you post has an id or not.\n\n"
                    + "* If you are creating a new group, then this body parameter should only specify **name** and **description**. \n"
                    + "* If you are editing an existing group, then it should only specify **id**, **name** and **description**.\n\n"
                    + "All other fields are automatically generated.",
            position = 3
    )
    @RequestMapping(value="/groups", method= RequestMethod.POST)
    public @ResponseBody
    Views.UserGroup postGroup(

            @ApiParam(value = "A json object representing the created or edited group", required = true)
            @RequestBody Views.UserGroup group,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,
            HttpSession session
    ) throws Unauthorized, Forbidden, NotFound, BadRequest {

        User user = getUser(apikey, session) ;

        if (group.getId() == null)
            throw new BadRequest("You must specify at least the group id") ;

        if (!idIsValid(group.getId()))
            throw new BadRequest("Your group id contains illegal characters") ;

        UserGroup existingGroup = userGroupRepo.findById(group.getId()) ;

        if (existingGroup != null) {
            checkGroupRole(user, group.getId(), GroupMember.Role.admin, "You must be an admin of the group to edit it") ;

            User creator = userRepo.findOne(existingGroup.getCreatorId()) ;

            UserGroup newGroup = new UserGroup(group.getId(), group.getName(), group.getDescription(), creator) ;
            userGroupRepo.save(newGroup) ;

            groupListenerManager.groupCreated(newGroup);

            return new Views.UserGroup(newGroup, creator) ;

        } else {

            checkGroupRole(user, "root", GroupMember.Role.user, "You must be a user or admin of the 'root' group to create new groups");

            UserGroup newGroup = new UserGroup(group.getId(), group.getName(), group.getDescription(), user) ;
            userGroupRepo.save(newGroup) ;

            GroupMember membership = new GroupMember(group, user, GroupMember.Role.admin) ;
            groupMemberRepo.save(membership) ;

            groupListenerManager.groupModified(newGroup) ;

            return new Views.UserGroup(newGroup, user) ;
        }
    }

    @ApiOperation(
            value = "Deletes a group",
            notes = "Deletes the given group, and all associated information.",
            position = 4
    )
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value="/groups/{groupId}", method= RequestMethod.DELETE)
    public void deleteGroup(

            @ApiParam(value = "The id of the group to delete", required = true)
            @PathVariable String groupId,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    )  throws Unauthorized, Forbidden, NotFound {

        User user = getUser(apikey, session) ;

        UserGroup group = userGroupRepo.findById(groupId) ;

        if (group == null)
            throw new NotFound("Group does not exist") ;

        super.checkGroupRole(user, groupId, GroupMember.Role.admin, "You must be an administrator of a group to delete it");

        userGroupRepo.remove(groupId) ;
        groupMemberRepo.removeAllForGroup(groupId) ;
        invitationRepo.removeAllForGroup(groupId) ;
        requestRepo.removeAllForGroup(groupId) ;

        groupListenerManager.groupDeleted(group);
    }


    private List<Views.UserGroup> hydrate(List<UserGroup> groups) {

        Map<String,User> cachedCreators = new HashMap<String,User>() ;

        List<Views.UserGroup> hydratedGroups = new ArrayList<Views.UserGroup>() ;

        for(UserGroup group:groups) {

            User creator = cachedCreators.get(group.getCreatorId()) ;
            if (creator == null) {
                creator = userRepo.findOne(group.getCreatorId()) ;
                cachedCreators.put(group.getCreatorId(), creator) ;
            }

            hydratedGroups.add(new Views.UserGroup(group, creator));
        }

        return hydratedGroups ;
    }


    private static boolean idIsValid(String id) {
        Matcher m = validGroupIdPattern.matcher(id) ;

        return m.matches() ;
    }


}
