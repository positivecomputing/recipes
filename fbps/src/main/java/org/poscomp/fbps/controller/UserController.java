package org.poscomp.fbps.controller;


import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.json.JsonArray;
import com.restfb.json.JsonObject;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.poscomp.fbps.error.Unauthorized;
import org.poscomp.fbps.model.FacebookProfile;
import org.poscomp.fbps.model.User;
import org.poscomp.fbps.model.Views;
import org.poscomp.fbps.repository.UserRepository;
import org.poscomp.fbps.util.FacebookUtils;
import org.poscomp.fbps.util.RandomKeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by dmilne on 7/06/2014.
 */
@Api(value = "users", position=1)
@Controller
public class UserController extends ControllerBase {

    private static Logger logger = LoggerFactory.getLogger(UserController.class) ;

    private RandomKeyGenerator keyGenerator = new RandomKeyGenerator();

    @Autowired
    private UserRepository userRepo ;

    @Autowired
    private FacebookUtils fb ;


    @ApiOperation(
            value = "Returns the caller's details",
            notes = "Returns the caller's details, including name, profile picture and apikey.",
            response = Views.UserWithApiKey.class
    )
    @RequestMapping(value="/users/me", method= RequestMethod.GET)
    public @ResponseBody Views.UserWithApiKey getCaller(

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized {
        User user = getUser(apikey, session) ;

        return new Views.UserWithApiKey(user) ;
    }


    @ApiOperation(
            value = "Regenerates a random API key for the caller",
            notes = "Regenerates a random API key for the caller, and then returns their details, including name, profile picture and the new apikey.",
            response = Views.UserWithApiKey.class
    )
    @RequestMapping(value="/users/me/resetKey", method= RequestMethod.GET)
    public @ResponseBody Views.UserWithApiKey resetApiKey(

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized {

        User user = getUser(apikey, session) ;

        user.setApiKey(keyGenerator.generate());

        userRepo.save(user) ;

        return new Views.UserWithApiKey(user) ;
    }

    @ApiOperation(
            value = "Returns facebook profiles that match the given query.",
            notes = "Returns facebook profiles that match the given query. Intended to help you locate potential users to add to groups.\n\n"
            + "Unfortunately you can only search by name (no narrowing down by age, gender, location, etc.) so it can be difficult to find specific people. "
            + "It may be best to ask new people to log in first, and then locate them using the **users/recent** service.",
            response = Views.User.class
    )
    @RequestMapping(value="/users/search", method= RequestMethod.GET)
    public @ResponseBody Collection<Views.User> search(

            @ApiParam(value = "The name of the facebook user to retrieve", required=true)
            @RequestParam String query,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized {

        User user = getUser(apikey, session) ;

        if (user.getToken() == null)
            throw new Unauthorized("No facebook access token. Please log in first") ;

        if (user.getTokenExpiry() != null && user.getTokenExpiry().before(new Date()))
            throw new Unauthorized("Your facebook access token has expired. Please log in to refresh it") ;

        FacebookClient client = fb.getUserClient(user.getToken()) ;

        List<Views.User> searchResults = new ArrayList<Views.User>() ;

        JsonObject rawSearchResults = client.fetchObject(
                "search",
                JsonObject.class,
                Parameter.with("q", query),
                Parameter.with("type", "user"),
                Parameter.with("fields", FacebookProfile.FIELDS),
                Parameter.with("limit", 20)
        ) ;

        JsonArray rawData = rawSearchResults.getJsonArray("data") ;

        for (int i=0 ; i<rawData.length() ; i++) {
            JsonObject rawProfile = rawData.getJsonObject(i);
            searchResults.add(new Views.User(rawProfile));
        }

        return searchResults ;
    }


    @ApiOperation(
            value = "Returns the profiles of people who recently logged in.",
            notes = "Returns facebook profiles of people who recently logged in. Intended to help you locate potential users to add to groups.\n\n"
                    + "This only returns the 20 profiles that signed in most recently, so use the **signedInBefore** filter to page through more users if necessary",
            response = Views.User.class
    )
    @RequestMapping(value="/users/recent", method= RequestMethod.GET)
    public @ResponseBody Collection<Views.User> getRecent(

            @ApiParam(value = "An optional filter, to only return users who last logged in before the given date")
            @RequestParam(required = false) Date signedInBefore,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized {

        User user = getUser(apikey, session) ;

        List<Views.User> recentProfiles = new ArrayList<Views.User>() ;

        for (User u: userRepo.findRecent(signedInBefore)) {

            if (u.getUserId().equals(user.getUserId()))
                continue ;

            if (u.getProfile() == null)
                continue ;

            if (recentProfiles.size() >= 20)
                break ;

            recentProfiles.add(new Views.User(u)) ;
        }

        return recentProfiles ;
    }



}
