package org.poscomp.fbps.controller;


import org.poscomp.fbps.model.GroupMember;
import org.poscomp.fbps.model.UserGroup;
import org.poscomp.fbps.repository.GroupMemberRepository;
import org.poscomp.fbps.repository.UserGroupRepository;
import org.poscomp.fbps.repository.UserRepository;
import org.poscomp.fbps.util.FacebookUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * Created by dmilne on 5/06/2014.
 */
@Component
@Scope(value = "singleton")
public class Initializer implements InitializingBean {

    private static Logger logger = LoggerFactory.getLogger(Initializer.class) ;

    @Autowired
    private UserRepository userRepo ;

    @Autowired
    private UserGroupRepository groupRepo ;

    @Autowired
    private GroupMemberRepository groupMemberRepo ;

    @Autowired
    private FacebookUtils fb ;

    @Override
    public void afterPropertiesSet() throws Exception {

        UserGroup rootGroup = groupRepo.getRoot() ;

        for (String adminId:fb.getAppAdministratorIds()) {

            GroupMember groupMember = groupMemberRepo.findByGroupIdAndUserId(rootGroup.getId(),adminId) ;
            if (groupMember == null)
                groupMember = new GroupMember(rootGroup,adminId, GroupMember.Role.admin) ;
            else
                groupMember.setRole(GroupMember.Role.admin);

            groupMemberRepo.save(groupMember) ;
        }

    }
}
