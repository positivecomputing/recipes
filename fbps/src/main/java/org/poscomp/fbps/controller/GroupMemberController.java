package org.poscomp.fbps.controller;


import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.poscomp.fbps.error.BadRequest;
import org.poscomp.fbps.error.Forbidden;
import org.poscomp.fbps.error.NotFound;
import org.poscomp.fbps.error.Unauthorized;
import org.poscomp.fbps.model.*;
import org.poscomp.fbps.repository.GroupMemberRepository;
import org.poscomp.fbps.repository.GroupMemberRequestRepository;
import org.poscomp.fbps.repository.UserGroupRepository;
import org.poscomp.fbps.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by dmilne on 23/06/2014.
 */
@Api(value = "group members", position = 3)
@Controller
public class GroupMemberController extends ControllerBase {

    @Autowired
    private UserRepository userRepo ;

    @Autowired
    private UserGroupRepository groupRepo;

    @Autowired
    private GroupMemberRepository memberRepo ;

    @Autowired
    private GroupMemberRequestRepository requestRepo ;

    @ApiOperation(
            value = "List all members of a group",
            notes = "List all members of the given group. This includes memberships involving specific people, and possibly one that applies to *anyone* if such a membership has been created.",
            response = Views.GroupMember.class,
            responseContainer = "List"
    )
    @RequestMapping(value="/groups/{groupId}/members", method= RequestMethod.GET)
    public @ResponseBody Collection<Views.GroupMember> getGroupMembers(

            @ApiParam(value = "The id of the group to retrieve members for", required=true)
            @PathVariable String groupId,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,
            HttpSession session
    ) throws Unauthorized, Forbidden, NotFound {

        User user = getUser(apikey, session) ;

        UserGroup group = groupRepo.findById(groupId) ;
        if (group == null)
            throw new NotFound("Group does not exist") ;

        checkGroupRole(user, groupId, GroupMember.Role.guest, "You must be a member of a group to view it's members");

        List<Views.GroupMember> groupMembers = new ArrayList<Views.GroupMember>() ;
        for (GroupMember gm: memberRepo.findByGroupId(group.getId())) {
            User u ;
            if (gm.getUserId().equals(GroupMember.ANY))
                u = User.ANYONE ;
            else
                u = userRepo.findOne(gm.getUserId()) ;


            groupMembers.add(new Views.GroupMember(gm, u)) ;
        }

        return groupMembers ;
    }


    @ApiOperation(
            value = "Returns the caller's membership to the given group",
            notes = "Returns the caller's membership to the given group. This may be a membership specific to them, or one assigned to *anyone*. This will always return the relevant membership that has the highest role.",
            response = Views.GroupMember.class
    )
    @RequestMapping(value="/groups/{groupId}/members/me", method= RequestMethod.GET)
    public @ResponseBody Views.GroupMember getMyMembership(
            @ApiParam(value = "The id of the group to retrieve your membership for", required=true)
            @PathVariable String groupId,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session) throws Unauthorized, Forbidden, NotFound {

        User user = getUser(apikey, session) ;

        GroupMember membership = memberRepo.findByGroupIdAndUserId(groupId, user.getUserId()) ;

        if (membership != null)
            return new Views.GroupMember(membership, user) ;

        membership = memberRepo.findByGroupIdAndUserId(groupId, GroupMember.ANY) ;

        if (membership != null)
            return new Views.GroupMember(membership, User.ANYONE) ;

        return null ;
    }



    @ApiOperation(
            value = "Returns the membership of an individual to the given group",
            notes = "Returns the membership of an individual to the given group. It can also return the membership that applies to *anyone*, if `\\*` is used as **userId**.",
            response = Views.GroupMember.class
    )
    @RequestMapping(value="/groups/{groupId}/members/{userId}", method= RequestMethod.GET)
    public @ResponseBody Views.GroupMember getGroupMember(

            @ApiParam(value = "The id of the group to retrieve member for", required=true)
            @PathVariable String groupId,

            @ApiParam(value = "The id of the user to retrieve membership for, or `\\*` to retrieve the membership that applies to *anyone* ", required=true)
            @PathVariable String userId,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized, Forbidden, NotFound {

        User user = getUser(apikey, session) ;

        checkGroupRole(user, groupId, GroupMember.Role.guest, "You must be a member of a group to view it's members");

        GroupMember membership = memberRepo.findByGroupIdAndUserId(groupId, userId) ;

        if (membership == null)
            membership = memberRepo.findByGroupIdAndUserId(groupId, GroupMember.ANY) ;

        User u ;
        if (userId.equals(GroupMember.ANY))
            u = User.ANYONE ;
        else
            u = userRepo.findOne(userId) ;

        return new Views.GroupMember(membership, u) ;
    }

    @ApiOperation(
            value = "Creates a new group membership, or edits an existing one",
            notes = "Either creates a new group membership or edits an existing one, depending on whether the **membership** object in the request body involves an existing user or not.\n\n"
                    + "* Whether you are creating or editing, this body parameter should only specify **groupId**, **id** and **role**. \n\n"
                    + "All other fields are automatically generated.\n\n",
            response = Views.GroupMember.class,
            position = 3
    )
    @RequestMapping(value="/groups/{groupId}/members", method= RequestMethod.POST)
    public @ResponseBody Views.GroupMember postGroupMembership(

            @ApiParam(value = "The id of the group to add/edit member for", required=true)
            @PathVariable String groupId,

            @ApiParam(value = "A json object representing the new membership to add, or the existing one to edit", required=true)
            @RequestBody Views.GroupMember membership,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,
            HttpSession session) throws Unauthorized, Forbidden, NotFound, BadRequest {

        User caller = getUser(apikey, session) ;

        UserGroup group = groupRepo.findById(groupId) ;
        if (group == null)
            throw new NotFound("Group does not exist") ;

        if (!membership.getGroupId().equals(groupId))
            throw new BadRequest("Group ids do not match") ;

        checkGroupRole(caller, groupId, GroupMember.Role.admin, "You must be an administrator of this group to edit members");


        User membershipUser = addOrUpdateUser(membership.getUserId(), caller) ;


        GroupMember existingMembership = memberRepo.findByGroupIdAndUserId(groupId, membership.getUserId()) ;

            if (membership.getUserId().equals(caller.getUserId()))
                throw new Forbidden("You cannot edit your own permissions") ;

            //see if this change resolves any existing request

            for (GroupMemberRequest pendingRequest:requestRepo.find(membershipUser, group, GroupMemberRequest.State.pending)) {

                if (pendingRequest.getRole().ordinal() >= membership.getRole().ordinal()) {
                    //new membership has a better or equal role than request, so consider it granted
                    pendingRequest.resolve(true, caller);
                    requestRepo.save(pendingRequest) ;
                }
            }


            if (existingMembership == null) {
                GroupMember newMembership = new GroupMember(group, membershipUser, membership.getRole()) ;
                memberRepo.save(newMembership) ;
                return new Views.GroupMember(newMembership, membershipUser) ;
            } else {
                existingMembership.setRole(membership.getRole()) ;
                memberRepo.save(existingMembership) ;
                return new Views.GroupMember(existingMembership, membershipUser) ;
            }

    }




    @ApiOperation(
            value = "Deletes a group membership",
            notes = "Deletes the given group membership",
            response = Views.GroupMember.class,
            position = 4
    )
    @RequestMapping(value="/groups/{groupId}/members/{userId}", method= RequestMethod.DELETE)
    public @ResponseBody Views.GroupMember deleteGroupMembership(

            @ApiParam(value = "The id of the group to delete membership from", required=true)
            @PathVariable String groupId,

            @ApiParam(value = "The id of the user who's membership to delete", required=true)
            @PathVariable String userId,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,
            HttpSession session) throws Unauthorized, Forbidden, NotFound, BadRequest {

        User user = getUser(apikey, session) ;

        if (user.getUserId().equals(userId))
            throw new Forbidden("You cannot edit your own permissions") ;

        checkGroupRole(user, groupId, GroupMember.Role.admin, "You must be an administrator of a group to remove members");

        GroupMember membership = memberRepo.findByGroupIdAndUserId(groupId, userId) ;
        if (membership == null)
            throw new NotFound(userId + " is not a member of " + groupId) ;


        User u ;
        if (userId.equals(GroupMember.ANY))
            u = User.ANYONE ;
        else
            u = userRepo.findOne(userId) ;

        memberRepo.remove(membership) ;
        return new Views.GroupMember(membership, u) ;
    }

}
