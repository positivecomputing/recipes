package org.poscomp.fbps.controller;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.poscomp.fbps.error.BadRequest;
import org.poscomp.fbps.error.Forbidden;
import org.poscomp.fbps.error.NotFound;
import org.poscomp.fbps.error.Unauthorized;
import org.poscomp.fbps.model.*;
import org.poscomp.fbps.repository.GroupMemberRepository;
import org.poscomp.fbps.repository.GroupMemberRequestRepository;
import org.poscomp.fbps.repository.UserGroupRepository;
import org.poscomp.fbps.repository.UserRepository;
import org.poscomp.fbps.util.FacebookUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dmilne on 25/08/2014.
 */
@Api(value = "group member requests", position = 4)
@Controller
public class GroupMemberRequestController extends ControllerBase{

    @Autowired
    private UserRepository userRepo ;

    @Autowired
    private UserGroupRepository groupRepo;

    @Autowired
    private GroupMemberRepository memberRepo ;

    @Autowired
    private GroupMemberRequestRepository requestRepo ;

    @Autowired
    private FacebookUtils facebook ;


    @RequestMapping(value="/groups/{groupId}/requests", method= RequestMethod.GET)
    public @ResponseBody List<Views.GroupMemberRequest> getRequestsForGroup(

        @ApiParam(value = "The id of the group to retrieve membership requests for", required=true)
        @PathVariable String groupId,

        @ApiParam(value = "An optional filter to restrict requests to those for the given user")
        @RequestParam(required = false) String userId,

        @ApiParam(value = "An optional filter to restrict requests to those with the given state")
        @RequestParam(required = false) GroupMemberRequest.State state,

        @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
        @RequestParam(required = false) String apikey,

        HttpSession session
    ) throws Unauthorized, Forbidden, NotFound {

        User caller = getUser(apikey, session) ;

        User user = null ;

        UserGroup group = groupRepo.findById(groupId) ;

        if (group == null)
            throw new NotFound("Group does not exist") ;

        GroupMember membership = memberRepo.findByGroupIdAndUserId(groupId, caller.getUserId()) ;

        if (userId != null) {
            if (userId.equals(User.ME))
                user = caller;
            else
                user = userRepo.findOne(userId);
        }

        List<GroupMemberRequest> requests ;
        if (membership == null || !membership.getRole().equals(GroupMember.Role.admin)) {

            if (user != null && !user.getUserId().equals(caller.getUserId()))
                throw new Forbidden("You are not an administrator of this group, so you can only see your own requests") ;

            //not an administrator, so can only see own requests
            requests = requestRepo.find(caller, group, state) ;

        } else {

            //is an administrator, so can see all requests
            requests = requestRepo.find(user, group, state) ;
        }

        return hydrate(requests) ;
    }



    @RequestMapping(value="/users/me/requests", method= RequestMethod.GET)
    public @ResponseBody List<Views.GroupMemberRequest> getRequestsForCaller(

            @ApiParam(value = "An optional filter to restrict requests to those with the given state")
            @RequestParam(required = false) GroupMemberRequest.State state,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized, Forbidden, NotFound {

        User caller = getUser(apikey, session) ;

        List<GroupMemberRequest> requests = requestRepo.find(caller, null, state) ;

        return hydrate(requests) ;
    }

    @RequestMapping(value="/groups/{groupId}/requests/{requestId}", method= RequestMethod.GET)
    public @ResponseBody Views.GroupMemberRequest getRequest(

            @ApiParam(value = "The id of the group to retrieve membership request for", required=true)
            @PathVariable String groupId,

            @ApiParam(value = "The id of the request to retrieve", required=true)
            @PathVariable String requestId,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized, Forbidden, NotFound, BadRequest {

        User caller = getUser(apikey, session) ;

        UserGroup group = groupRepo.findById(groupId) ;

        if (group == null)
            throw new NotFound("Group does not exist") ;

        if (!ObjectId.isValid(requestId))
            throw new BadRequest("Request id is invalid") ;

        GroupMemberRequest request = requestRepo.findById(new ObjectId(requestId)) ;

        GroupMember membership = memberRepo.findByGroupIdAndUserId(groupId, caller.getUserId()) ;

        if (membership == null || !membership.getRole().equals(GroupMember.Role.admin)) {

            //not an administrator, so can only see own requests

            if (!request.getUserId().equals(caller.getUserId()))
                throw new Forbidden("You are not an administrator of this group, so you can only view your own membership requests") ;
        }

        return hydrate(request) ;
    }

    @RequestMapping(value="/groups/{groupId}/requests", method= RequestMethod.POST)
    private @ResponseBody GroupMemberRequest postRequest(

            @ApiParam(value = "The id of the group to retrieve membership requests for", required=true)
            @PathVariable String groupId,

            @ApiParam(value = "A json object representing the new request to create, or the existing one to edit", required=true)
            @RequestBody Views.GroupMemberRequest request,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized, Forbidden, NotFound, BadRequest {

        User caller = getUser(apikey, session) ;

        UserGroup group = groupRepo.findById(groupId) ;

        if (group == null)
            throw new NotFound("Group does not exist") ;

        GroupMember membership = memberRepo.findByGroupIdAndUserId(groupId, caller.getUserId()) ;

        boolean isAdmin = (membership != null && membership.getRole().equals(GroupMember.Role.admin)) ;

        if (request.getId() != null) {

            //we are editing (granting or denying) an existing request

            GroupMemberRequest existingRequest = requestRepo.findById(request.getId()) ;

            if (existingRequest == null)
                throw new NotFound("The request you are trying to edit does not exist. Do not specify ids for new requests") ;

            if (!existingRequest.getState().equals(GroupMemberRequest.State.pending))
                throw new Forbidden("Only pending requests may be edited") ;

            if (!isAdmin)
                throw new Forbidden("Only administrators can edit requests.") ;

            switch (request.getState()) {
                case granted:
                    existingRequest.resolve(true, caller) ;

                    User user = addOrUpdateUser(existingRequest.getUserId(), caller) ;

                    GroupMember member = new GroupMember(group, user, existingRequest.getRole()) ;
                    memberRepo.save(member) ;

                    break ;

                case denied:

                    existingRequest.resolve(false, caller) ;

                    break ;
                default:
                    throw new BadRequest("You must either grant or deny the request") ;
            }

            requestRepo.save(existingRequest) ;
            return existingRequest ;


        } else {

            //we are creating a new request

            if (request.getRole() == null)
                throw new BadRequest("The request does not specify a role") ;

            if (request.getUser().getUserId() != null && !request.getUser().getUserId().equals(caller.getUserId()))
                throw new Forbidden("You cannot create requests on behalf of someone else") ;

            if (membership !=null && membership.getRole().ordinal() <= request.getRole().ordinal())
                throw new BadRequest("You already have this role") ;

            List<GroupMemberRequest> collidingRequests = requestRepo.find(caller, group, GroupMemberRequest.State.pending) ;
            if (collidingRequests.size() > 0)
                throw new Forbidden("You already have a request pending for this group") ;

            GroupMemberRequest newRequest = new GroupMemberRequest(caller, group, request.getRole()) ;
            requestRepo.save(newRequest) ;

            return newRequest ;

        }





    }




    private List<Views.GroupMemberRequest> hydrate(List<GroupMemberRequest> requests) {

        List<Views.GroupMemberRequest> hydratedRequests = new ArrayList<Views.GroupMemberRequest>() ;

        Map<String,User> cachedUsers = new HashMap<String,User>() ;
        Map<String,UserGroup> cachedGroups = new HashMap<String,UserGroup>() ;

        for(GroupMemberRequest request:requests) {

            User user = cachedUsers.get(request.getUserId()) ;
            if (user == null) {
                user = userRepo.findOne(request.getUserId());
                cachedUsers.put(user.getUserId(), user) ;
            }

            UserGroup group = cachedGroups.get(request.getGroupId()) ;
            if (group == null) {
                group = groupRepo.findById(request.getGroupId());
                cachedGroups.put(group.getId(), group) ;
            }

            User groupCreator = cachedUsers.get(group.getCreatorId()) ;
            if (groupCreator == null) {
                groupCreator = userRepo.findOne(group.getCreatorId()) ;
                cachedUsers.put(group.getCreatorId(), groupCreator) ;
            }

            User resolvedBy = null ;
            if (request.getResolvedBy() != null) {

                resolvedBy = cachedUsers.get(request.getResolvedBy()) ;
                if (resolvedBy == null) {
                    resolvedBy = userRepo.findOne(request.getUserId());
                    cachedUsers.put(user.getUserId(), resolvedBy) ;
                }
            }

            Views.GroupMemberRequest hydratedRequest = new Views.GroupMemberRequest(request, user, group, groupCreator, resolvedBy) ;
            hydratedRequests.add(hydratedRequest) ;
        }

        return hydratedRequests ;

    }

    private Views.GroupMemberRequest hydrate(GroupMemberRequest request) {

        User user = userRepo.findOne(request.getUserId());
        UserGroup group =  groupRepo.findById(request.getGroupId());
        User groupCreator = userRepo.findOne(group.getCreatorId()) ;

        User resolvedBy = null ;
        if (request.getResolvedBy() != null)
            resolvedBy = userRepo.findOne(request.getUserId());

        return new Views.GroupMemberRequest(request, user, group, groupCreator, resolvedBy) ;
    }







}
