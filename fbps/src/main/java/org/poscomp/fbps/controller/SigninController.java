package org.poscomp.fbps.controller;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.json.JsonObject;
import org.apache.http.client.utils.URIBuilder;
import org.poscomp.fbps.error.Unauthorized;
import org.poscomp.fbps.model.FacebookProfile;
import org.poscomp.fbps.model.User;
import org.poscomp.fbps.repository.UserRepository;
import org.poscomp.fbps.util.FacebookUtils;
import org.poscomp.fbps.util.RandomKeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URI;

/**
 * Created by dmilne on 5/06/2014.
 */
@Controller
public class SigninController extends ControllerBase implements InitializingBean {

    private static final String RETURN_TO = "return_to" ;

    private static final String RETURN_WITH_APIKEY = "return_with_api_key" ;

    private static final String STATE = "fb_state" ;

    private static Logger logger = LoggerFactory.getLogger(SigninController.class) ;

    @Autowired
    private FacebookUtils fb ;

    @Autowired
    private UserRepository userRepo ;

    private ObjectMapper jsonMapper ;

    private RandomKeyGenerator keyGenerator ;

    @Override
    public void afterPropertiesSet() throws Exception {
        jsonMapper = new ObjectMapper();
        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        keyGenerator = new RandomKeyGenerator() ;
    }

    @ApiIgnore
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logout(HttpSession session) {

        this.setUser(null, session);

        return "redirect:" + getServerUrl() ;
    }

    @ApiIgnore
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String login(
            @RequestParam(defaultValue=".") String returnTo,
            @RequestParam(defaultValue="false") boolean returnWithApiKey,
            HttpSession session,
            HttpServletRequest request
    ) {

        logger.info("HANDING LOGIN REQUEST") ;

        session.setAttribute(RETURN_TO, returnTo);
        session.setAttribute(RETURN_WITH_APIKEY, returnWithApiKey);

        try {
            User user = getUser(null, session);

            //if this didn't throw an exception, then we are logged in already ;
            return "redirect:" + getReturnTo(session, user);
        } catch (Unauthorized u) {
            //session.setAttribute(RETURN_TO, returnTo);

            String stateKey = keyGenerator.generate() ;
            session.setAttribute(STATE, stateKey);

            return "redirect:" + fb.getAuthenticationURI(getCallbackUrl(), stateKey);
        }
    }

    @ApiIgnore
    @RequestMapping(value="/callback", method=RequestMethod.GET)
    public String handleCallback(
            @RequestParam(required = false) String code,
            @RequestParam(required = false) String error,
            @RequestParam(required = false) String error_reason,
            HttpSession session,
            HttpServletRequest request
    ) throws IOException {

        logger.info("HANDING CALLBACK") ;

        if (error != null) {
            logger.warn(error + " " + error_reason) ;
            return "redirect:" + getReturnTo(session, null) ;
        }


        logger.info("code " + code) ;

        //I have no idea why this requires a callback url, this is supposed to be server-to-server
        FacebookClient.AccessToken shortTermToken = fb.getFacebookUserToken(code, getCallbackUrl());

        logger.info("user short-term token:" + shortTermToken.getAccessToken()) ;

        FacebookClient appClient = fb.getAppClient() ;

        logger.info("url:" + request.getContextPath()) ;

        FacebookClient.AccessToken longTermToken = appClient.obtainExtendedAccessToken(fb.getClientId(), fb.getClientSecret(), shortTermToken.getAccessToken()) ;
        logger.info("user long-term token:" + longTermToken.getAccessToken()) ;

        FacebookClient userClient = fb.getUserClient(longTermToken.getAccessToken()) ;

        JsonObject rawProfile = userClient.fetchObject("me", JsonObject.class, Parameter.with("fields", FacebookProfile.FIELDS));
        FacebookProfile profile = new FacebookProfile(rawProfile) ;

        User user = userRepo.findOne(profile.getUserId()) ;

        if (user == null) {
            user = new User(profile) ;
            user.setApiKey(keyGenerator.generate());
            user.updateToken(longTermToken); ;
        } else {
            user.updateProfile(profile);
            user.updateToken(longTermToken);

            if (user.getApiKey() == null)
                user.setApiKey(keyGenerator.generate());
        }

        user.registerLogin();

        userRepo.save(user) ;

        setUser(user, session);

        return "redirect:" + getReturnTo(session, user);
    }



    private String getCallbackUrl() {

        return getServerUrl() + "/callback" ;

    }


    private String getReturnTo(HttpSession session, User user) {



        try {

            Object rt = session.getAttribute(RETURN_TO);

            if (rt == null)
                return getServerUrl() + "/" ;

            String returnTo = (String) rt ;

            URI returnToUri = new URI(returnTo);
            if (!returnToUri.isAbsolute())
                returnToUri = new URI(getServerUrl() + "/" + returnTo) ;


            URIBuilder builder = new URIBuilder(returnToUri) ;

            boolean returnWithApiKey = false ;
            Object rwa = session.getAttribute(RETURN_WITH_APIKEY) ;
            if (rwa != null)
                returnWithApiKey = (Boolean)rwa ;

            if (returnWithApiKey && user != null)
                builder.addParameter("apikey", user.getApiKey()) ;

            return builder.build().toString() ;

        } catch (Exception e) {

            logger.warn("Could not construct return to url", e) ;
            return getServerUrl() + "/" ;
        }

    }


}
