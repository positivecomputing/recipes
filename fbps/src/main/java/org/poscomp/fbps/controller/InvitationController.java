package org.poscomp.fbps.controller;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.poscomp.fbps.error.BadRequest;
import org.poscomp.fbps.error.Forbidden;
import org.poscomp.fbps.error.NotFound;
import org.poscomp.fbps.error.Unauthorized;
import org.poscomp.fbps.model.*;
import org.poscomp.fbps.repository.GroupMemberRepository;
import org.poscomp.fbps.repository.UserGroupRepository;
import org.poscomp.fbps.repository.UserRepository;
import org.poscomp.fbps.repository.InvitationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dmilne on 5/11/14.
 */
@Controller
@Api(value = "group member invitations", position = 4)
public class InvitationController extends ControllerBase {

    @Autowired
    private UserRepository userRepo ;

    @Autowired
    private UserGroupRepository groupRepo;

    @Autowired
    private GroupMemberRepository memberRepo ;

    @Autowired
    private InvitationRepository invitationRepo ;


    @RequestMapping(value="/groups/{groupId}/invitations", method= RequestMethod.GET)
    public @ResponseBody
    List<Views.Invitation> getInvitationsForGroup(

            @ApiParam(value = "The id of the group to retrieve invitations for", required=true)
            @PathVariable String groupId,

            @ApiParam(value = "An optional filter to restrict invitations to those with the given state")
            @RequestParam(required = false) Invitation.State state,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized, Forbidden, NotFound {

        User caller = getUser(apikey, session) ;

        UserGroup group = groupRepo.findById(groupId) ;

        if (group == null)
            throw new NotFound("Group does not exist") ;

        super.checkGroupRole(caller, groupId, GroupMember.Role.admin, "You must be an administrator of the group to view invitations");

        List<Invitation> invitations ;

        return hydrate(invitationRepo.findAll(groupId, state)) ;

    }


    @RequestMapping(value="/invitations/{invitationId}", method= RequestMethod.GET)
    public @ResponseBody Views.Invitation getInvitation(
            @ApiParam(value = "The id of the invitation to retrieve", required=true)
            @PathVariable String invitationId
    ) throws Unauthorized, Forbidden, BadRequest, NotFound {

        if (!ObjectId.isValid(invitationId))
            throw new BadRequest("Invalid invitation id") ;

        Invitation invitation = invitationRepo.findOne(new ObjectId(invitationId)) ;

        if (invitation == null)
            throw new NotFound("Invitation does not exist") ;

        return hydrate(invitation) ;

    }

    @RequestMapping(value="/invitations/", method= RequestMethod.POST)
    public @ResponseBody
    Views.Invitation postInvitation(

            @ApiParam(value = "A json object representing the new invitation to create, or the existing one to edit", required=true)
            @RequestBody Views.Invitation invitation,

            @ApiParam(value = "An optional api key to identify the caller without them having to manually log in")
            @RequestParam(required = false) String apikey,

            HttpSession session
    ) throws Unauthorized, Forbidden, BadRequest, NotFound {

        User caller = getUser(apikey, session) ;




        if (invitation.getId() == null) {

            //this is a new invitation

            if (invitation.getRole() == null)
                throw new BadRequest("Invitation does not specify a role") ;

            if (invitation.getGroup() == null || invitation.getGroup().getId() == null)
                throw new BadRequest("Invitation does not specify a group") ;

            UserGroup group = groupRepo.findById(invitation.getGroup().getId()) ;
            if (group == null)
                throw new NotFound("Group does not exist") ;

            super.checkGroupRole(caller, group.getId(), GroupMember.Role.admin, "You must be an administrator to create new invitations") ;

            Invitation newInvitation = new Invitation(group, invitation.getRole(), caller) ;

            invitationRepo.save(newInvitation) ;

            return hydrate(newInvitation) ;

        } else {

            //this is an existing invitation

            Invitation existingInvitation = invitationRepo.findOne(invitation.getId()) ;

            if (existingInvitation == null)
                throw new NotFound("Invitation does not exist. Do not specify ids for new invitations") ;

            if (existingInvitation.getState() != Invitation.State.pending)
                throw new BadRequest("Existing invitation has already been used") ;



            UserGroup group = groupRepo.findById(existingInvitation.getGroupId()) ;
            GroupMember membership = memberRepo.findByGroupIdAndUserId(group.getId(), caller.getUserId()) ;

            if (membership != null && membership.getRole().ordinal() <= invitation.getRole().ordinal())
                throw new BadRequest("You already have the invited privileges") ;


            switch (existingInvitation.getState()) {



                case accepted:

                    if (membership == null)
                        membership = new GroupMember(group, caller, invitation.getRole()) ;
                    else
                        membership.setRole(invitation.getRole());

                    memberRepo.save(membership) ;

                    existingInvitation.resolve(true, caller);
                    break ;

                case declined:

                    existingInvitation.resolve(false, caller);
                    break ;

                default:

                    throw new BadRequest("You must either accept or decline the request") ;
            }


            invitationRepo.save(existingInvitation) ;
            return hydrate(existingInvitation) ;
        }

    }

    private void handleAccept(Views.Invitation invitation, UserGroup group, User caller) {

        GroupMember membership = memberRepo.findByGroupIdAndUserId(group.getId(), caller.getUserId()) ;


    }





    private List<Views.Invitation> hydrate(List<Invitation> invitations) {

        List<Views.Invitation> hydratedInvitations = new ArrayList<Views.Invitation>() ;

        if (invitations.isEmpty())
            return hydratedInvitations ;

        Map<String,UserGroup> groupCache = new HashMap<String,UserGroup>()  ;
        Map<String,User> userCache = new HashMap<String,User>() ;

        for (Invitation invitation:invitations) {

            UserGroup group = groupCache.get(invitation.getGroupId()) ;
            if (group == null) {
                group = groupRepo.findById(invitation.getGroupId()) ;
                groupCache.put(invitation.getGroupId(), group) ;
            }

            User groupCreator = userCache.get(group.getCreatorId()) ;
            if (groupCreator == null) {
                groupCreator = userRepo.findOne(group.getCreatorId()) ;
                userCache.put(group.getCreatorId(), groupCreator) ;
            }

            User invitationCreator = userCache.get(invitation.getCreatedBy()) ;
            if (invitationCreator == null) {
                invitationCreator = userRepo.findOne(invitation.getCreatedBy()) ;
                userCache.put(invitation.getCreatedBy(), invitationCreator) ;
            }

            User invitationUser = null ;
            if (invitation.getUsedBy() != null) {
                invitationUser = userCache.get(invitation.getUsedBy()) ;
                if (invitationUser == null) {
                    invitationUser = userRepo.findOne(invitation.getUsedBy()) ;
                    userCache.put(invitation.getUsedBy(), invitationUser) ;
                }
            }

            hydratedInvitations.add(new Views.Invitation(invitation, group, groupCreator, invitationCreator, invitationUser)) ;
        }


        return hydratedInvitations ;
    }

    private Views.Invitation hydrate(Invitation invitation) {

        UserGroup group = groupRepo.findById(invitation.getGroupId()) ;

        User groupCreator = userRepo.findOne(group.getCreatorId()) ;

        User invitationCreator = userRepo.findOne(invitation.getCreatedBy()) ;

        User invitationUser = null ;
        if (invitation.getUsedBy() != null)
            invitationUser = userRepo.findOne(invitation.getUsedBy()) ;

        return new Views.Invitation(invitation, group, groupCreator, invitationCreator, invitationUser) ;

    }

}
