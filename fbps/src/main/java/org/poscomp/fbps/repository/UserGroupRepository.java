package org.poscomp.fbps.repository;

import org.poscomp.fbps.model.UserGroup;
import org.poscomp.fbps.util.QueryParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by dmilne on 4/06/2014.
 */
@Repository
public class UserGroupRepository {

    public static final String rootId = "root" ;
    private static final String rootTitle = "Root" ;
    private static final String rootDescription =
            "This group provides members with advanced privileges over other groups. \n\n" +
            "* **users** are are able to create new groups.\n" +
            "* **admins** can create new groups, *and* are treated as admins in all other groups.\n" ;

    @Autowired
    private MongoTemplate m;

    public UserGroup findById(String id) {
        return m.findById(id, UserGroup.class) ;
    }

    public List<UserGroup> findByCreatorId(String creatorId) {
        return m.find(Query.query(Criteria.where("creatorId").is(creatorId)), UserGroup.class) ;
    }

    public List<UserGroup> findByQuery(String query) {

        List<String> phrases = QueryParser.parse(query) ;



        TextCriteria criteria = TextCriteria.forDefaultLanguage() ;
        for (String phrase:phrases) {

            if (phrase.indexOf(" ") > 0)
                criteria.matching(phrase) ;
            else
                criteria.matchingPhrase(phrase) ;
        }

        Query q = TextQuery.queryText(criteria).sortByScore() ;

        return m.find(q, UserGroup.class) ;
    }

    public UserGroup save(UserGroup group) {
        m.save(group) ;

        return group ;
    }

    public void remove(String groupId) {

        Query q = new Query().addCriteria(Criteria.where("id").is(groupId)) ;
        m.remove(q, UserGroup.class) ;
    }

    public UserGroup getRoot() {

        UserGroup root = findById(rootId) ;

        if (root == null)
            root = new UserGroup(rootId, rootTitle, rootDescription, "unknown") ;

        m.save(root) ;

        return root ;
    }

}