package org.poscomp.fbps.repository;

import org.bson.types.ObjectId;
import org.poscomp.fbps.model.Invitation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dmilne on 5/11/14.
 */
@Repository
public class InvitationRepository {

    @Autowired
    private MongoTemplate m;

    public Invitation findOne(ObjectId id) {
        return m.findById(id, Invitation.class) ;
    }

    public List<Invitation> findAll(String groupId, Invitation.State state) {

        Query q = new Query().addCriteria(Criteria.where("groupId").is(groupId)) ;

        if (state != null)
            q.addCriteria(Criteria.where("state").is(state)) ;

        return m.find(q, Invitation.class) ;

    }

    public void remove(Invitation invitation) {
        m.remove(invitation) ;
    }

    public Invitation save(Invitation invitation) {
        m.save(invitation) ;
        return invitation ;
    }


    public void removeAllForGroup(String groupId) {
        m.remove(new Query(Criteria.where("groupId").is(groupId))) ;
    }

}
