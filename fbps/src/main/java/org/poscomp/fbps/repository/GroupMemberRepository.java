package org.poscomp.fbps.repository;

import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.poscomp.fbps.model.GroupMember;
import org.poscomp.fbps.model.UserGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by dmilne on 4/06/2014.
 */
@Repository
public class GroupMemberRepository {

    private static Logger logger = LoggerFactory.getLogger(GroupMemberRepository.class) ;

    @Autowired
    private MongoTemplate m;

    public GroupMember findById(ObjectId id) {

        return m.findById(id, GroupMember.class) ;
    }

    public List<GroupMember> findByGroupId(String groupId) {

        Query q = new Query() ;
        q.addCriteria(Criteria.where("groupId").is(groupId)) ;

        return m.find(q, GroupMember.class) ;
    }


    public List<GroupMember> findByUserIdAndMinimumRole(String userId, GroupMember.Role role) {

        Set<GroupMember.Role> validRoles = new HashSet<GroupMember.Role>() ;

        if (role == null)
            role = GroupMember.Role.guest ;

        for (GroupMember.Role r: GroupMember.Role.values()) {
            if (r.ordinal() <= role.ordinal())
                validRoles.add(r) ;
        }

        logger.info("checking roles: " + StringUtils.join(validRoles, ",")) ;

        Query q = new Query() ;
        q.addCriteria(Criteria.where("userId").is(userId)) ;
        q.addCriteria(Criteria.where("role").in(validRoles)) ;

        return m.find(q, GroupMember.class) ;
    }

    public GroupMember findByGroupIdAndUserId(String groupId, String userId) {

        Query q = new Query() ;
        q.addCriteria(Criteria.where("userId").is(userId)) ;
        q.addCriteria(Criteria.where("groupId").is(groupId)) ;

        return m.findOne(q, GroupMember.class) ;
    }

    public GroupMember save(GroupMember groupMember) {
        m.save(groupMember) ;

        return groupMember ;
    }

    public void remove(GroupMember groupMember) {

        m.remove(groupMember) ;
    }

    public void removeAllForGroup(String groupId) {

        Query q = new Query().addCriteria(Criteria.where("groupId").is(groupId)) ;
        m.remove(q, GroupMember.class) ;
    }

}