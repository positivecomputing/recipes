package org.poscomp.fbps.repository;

import org.poscomp.fbps.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


/**
 * Created by dmilne on 5/06/2014.
 */
@Repository
public class UserRepository  {

    @Autowired
    private MongoTemplate m;

    public User findOne(String id) {
        return m.findById(id, User.class) ;
    }

    public User findByApiKey(String apiKey) {

        Query q = new Query().addCriteria(Criteria.where("apiKey").is(apiKey)) ;

        return m.findOne(q, User.class) ;
    }

    public User save(User user) {
        m.save(user) ;

        return user ;
    }

    public List<User> findRecent(Date signedInBefore) {

        Query q = new Query()
                .with(new Sort(Sort.Direction.DESC, "lastLogin"))
                .limit(20) ;

        if (signedInBefore != null)
            q.addCriteria(Criteria.where("lastLogin").lt(signedInBefore)) ;

        return m.find(q, User.class) ;
    }


}
