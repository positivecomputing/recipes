package org.poscomp.fbps.repository;

import org.bson.types.ObjectId;
import org.poscomp.fbps.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dmilne on 25/08/2014.
 */
@Repository
public class GroupMemberRequestRepository {

    private static Logger logger = LoggerFactory.getLogger(GroupMemberRequestRepository.class) ;

    @Autowired
    private MongoTemplate m;

    public GroupMemberRequest findById(ObjectId id) {

        return m.findById(id, GroupMemberRequest.class) ;
    }

    public List<GroupMemberRequest> find(User user, UserGroup group, GroupMemberRequest.State state) {

        Query query = new Query() ;

        if (user != null)
            query.addCriteria(Criteria.where("userId").is(user.getUserId())) ;

        if (group != null)
            query.addCriteria(Criteria.where("groupId").is(group.getId())) ;

        if (state != null)
            query.addCriteria(Criteria.where("state").is(state)) ;

        return m.find(query, GroupMemberRequest.class) ;
    }


    public GroupMemberRequest save(GroupMemberRequest request) {
        m.save(request) ;

        return request ;
    }

    public void removeAllForGroup(String groupId) {
        m.remove(new Query(Criteria.where("groupId").is(groupId))) ;
    }

}
