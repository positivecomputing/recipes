package org.poscomp.fbps.error;

/**
 * Created by dmilne on 5/06/2014.
 */
public class Forbidden extends Exception {

    public Forbidden(String message) {
        super(message) ;
    }

    public Forbidden(String message, Throwable cause) {
        super(message, cause) ;
    }
}
