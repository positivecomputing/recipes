package org.poscomp.fbps.error;

/**
 * Created by dmilne on 5/06/2014.
 */
public class Unauthorized extends Exception {

    public Unauthorized(String message) {
        super(message) ;
    }

    public Unauthorized(String message, Throwable cause) {
        super(message, cause) ;
    }
}
