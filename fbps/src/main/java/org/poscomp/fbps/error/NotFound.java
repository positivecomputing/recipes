package org.poscomp.fbps.error;

/**
 * Created by dmilne on 5/06/2014.
 */
public class NotFound extends Exception {

    public NotFound(String message) {
        super(message) ;
    }

    public NotFound(String message, Throwable cause) {
        super(message, cause) ;
    }
}
