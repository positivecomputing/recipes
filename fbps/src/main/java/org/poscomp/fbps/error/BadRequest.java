package org.poscomp.fbps.error;

/**
 * Created by dmilne on 6/06/2014.
 */
public class BadRequest extends Exception {

    public BadRequest(String message) {
        super(message) ;
    }

    public BadRequest(String message, Throwable cause) {
        super(message, cause) ;
    }
}
