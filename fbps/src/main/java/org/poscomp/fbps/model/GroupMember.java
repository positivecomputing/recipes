package org.poscomp.fbps.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by dmilne on 5/06/2014.
 */
@Document
public class GroupMember implements Comparable<GroupMember> {



    //admin - can add new members to the group, and do anything user can do
    //user - has more rights than guest (depends on application)
    //guest - lowest privilages (depends on application)

    public enum Role {admin, user, guest} ;

    public static final String ANY = "*" ;

    @Id
    private ObjectId memberId ;

    @Indexed
    private String groupId ;

    @Indexed
    private String userId ;

    @Indexed
    private Role role ;

    @SuppressWarnings(value="unused")
    private GroupMember() {
        //required by jackson
    }

    public GroupMember(UserGroup group, User user, Role role) {
        this.groupId = group.getId() ;

        if (user != null)
            this.userId = user.getUserId() ;
        else
            this.userId = ANY ;

        this.role = role ;
    }

    public GroupMember(UserGroup group, String userId, Role role) {
        this.groupId = group.getId() ;

        if (userId != null)
            this.userId = userId ;
        else
            this.userId = ANY ;

        this.role = role ;
    }

    public ObjectId getMemberId() {
        return memberId;
    }

    public String getGroupId() {
        return groupId ;
    }

    public String getUserId() {
        return userId;
    }

    public Role getRole() {

        return role ;
    }

    public void setRole(Role role) {
        this.role = role ;
    }


    public void resolveRequest(boolean grant, User resolvedBy) {


    }


    @Override
    public int compareTo(GroupMember g) {

        int cmp = getRole().compareTo(g.getRole());
        if (cmp != 0)
            return 0 ;

        cmp = userId.compareTo(g.userId) ;
        if (cmp != 0)
            return 0 ;

        return memberId.compareTo(g.memberId) ;
    }



}
