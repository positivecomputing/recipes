package org.poscomp.fbps.model;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

import java.util.Date;


/**
 * Created by dmilne on 4/06/2014.
 */
@Document
@ApiModel(description="A collection of users, who have varying rights to view and modify resources")
public class UserGroup {



    @Id
    private String id ;

    @TextIndexed(weight=2)
    private String name ;

    @TextIndexed
    private String description ;

    @Indexed
    private String creatorId ;

    private Date creationDate ;


    public UserGroup() {
        //required by jackson
    }



    public UserGroup(String id, String name, String description, String creatorId) {

        this.id = id ;
        this.name = name ;
        this.description = description ;

        if (creatorId != null)
            this.creatorId = creatorId ;
        else
            this.creatorId = "unknown" ;

        this.creationDate = new Date() ;
    }


    public UserGroup(String id, String name, String description, User creator) {

        this.id = id ;
        this.name = name ;
        this.description = description ;

        if (creator != null)
            this.creatorId = creator.getUserId() ;
        else
            this.creatorId = "unknown" ;

        this.creationDate = new Date() ;
    }

    public UserGroup(UserGroup group) {

        this.id = group.id ;
        this.name = group.name ;
        this.description = group.description ;
        this.creatorId = group.creatorId ;
        this.creationDate = group.creationDate ;
    }

    @ApiModelProperty(value="A unique id for the group")
    public String getId() {
        return id ;
    }

    @ApiModelProperty(value="A name for the group (not guaranteed to be unique)")
    public String getName() {
        return name;
    }

    @ApiModelProperty(value="A short description, formatted with markdown syntax")
    public String getDescription() {
        return description;
    }

    @ApiModelProperty(value="The id of the user who created this group")
    public String getCreatorId() {
        return creatorId;
    }

    @ApiModelProperty(value="The date this group was created")
    public Date getCreationDate() {
        return creationDate;
    }


}
