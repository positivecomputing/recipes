package org.poscomp.fbps.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by dmilne on 5/11/14.
 */
@Document
public class Invitation {

    public enum State {pending, accepted, declined}

    @Id
    private ObjectId id ;

    @Indexed
    private String groupId ;

    private GroupMember.Role role ;

    @Indexed
    private State state ;

    private String createdBy ;

    private Date createdAt ;

    private String usedBy ;

    private Date usedAt ;

    private Invitation() {


    }

    public Invitation(Invitation invitation) {
        this.id = invitation.id ;
        this.groupId = invitation.groupId ;
        this.role = invitation.role ;
        this.state = invitation.state ;
        this.createdBy = invitation.createdBy ;
        this.createdAt = invitation.createdAt ;
        this.usedBy = invitation.usedBy ;
        this.usedAt = invitation.usedAt ;

    }

    public Invitation(UserGroup group, GroupMember.Role role, User creator) {

        this.groupId = group.getId() ;
        this.role = role ;
        this.state = State.pending ;
        this.createdBy = creator.getUserId() ;
        this.createdAt = new Date() ;
    }

    public void resolve(boolean accept, User user) {

        this.usedBy = user.getUserId() ;
        this.usedAt = new Date() ;

        if (accept)
            this.state = State.accepted ;
        else
            this.state = State.declined ;

    }

    public ObjectId getId() {
        return id;
    }

    public String getGroupId() {
        return groupId;
    }

    public GroupMember.Role getRole() {
        return role;
    }

    public State getState() {
        return state;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getUsedBy() {
        return usedBy;
    }

    public Date getUsedAt() {
        return usedAt;
    }
}
