package org.poscomp.fbps.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by dmilne on 25/08/2014.
 */
@Document
public class GroupMemberRequest {

    public enum State {pending, granted, denied} ;

    @Id
    private ObjectId id ;

    @Indexed
    private String userId ;

    @Indexed
    private String groupId ;

    private GroupMember.Role role ;

    @Indexed
    private State state ;



    private Date requestedAt ;

    private Date resolvedAt ;

    private String resolvedBy ;

    private GroupMemberRequest() {

    }

    public GroupMemberRequest(User user, UserGroup group, GroupMember.Role role) {
        this.userId= user.getUserId() ;
        this.groupId = group.getId() ;
        this.role = role ;

        this.state = State.pending ;
        this.requestedAt = new Date() ;
    }

    public ObjectId getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public GroupMember.Role getRole() {
        return role;
    }

    public State getState() {
        return state;
    }

    public Date getRequestedAt() {
        return requestedAt;
    }

    public Date getResolvedAt() {
        return resolvedAt;
    }

    public String getResolvedBy() {
        return resolvedBy ;
    }

    public void resolve(boolean grant, User resolvedBy) {
        if (grant)
            this.state = State.granted ;
        else
            this.state = State.denied ;

        this.resolvedAt = new Date() ;
        this.resolvedBy = resolvedBy.getUserId() ;
    }
}
