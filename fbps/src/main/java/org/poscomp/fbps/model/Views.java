package org.poscomp.fbps.model;

import com.restfb.json.JsonObject;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

/**
 * Created by dmilne on 30/06/2014.
 */
public class Views {

    @ApiModel(description="Details of a user, retrieved via Facebook")
    public static class User extends FacebookProfile {

        @SuppressWarnings(value="unused")
        private User() {
            //needed by jackson
        }

        public User(org.poscomp.fbps.model.User user) {
            super(user.getProfile()) ;
        }

        public User(JsonObject rawProfile) {
            super(rawProfile) ;
        }
    }

    @ApiModel(description="Details of a user, including their API key")
    public static class UserWithApiKey extends FacebookProfile {

        private String apiKey ;

        @SuppressWarnings(value="unused")
        private UserWithApiKey() {
            //needed by jackson
        }
        public UserWithApiKey(org.poscomp.fbps.model.User user) {

            super(user.getProfile()) ;
            this.apiKey = user.getApiKey() ;
        }


        @ApiModelProperty(value="A key that must be used whenever this user wishes to use the REST api outside of an authenticated browser session", position=6)
        public String getApiKey() {
            return apiKey;
        }
    }

    @ApiModel(description="A collection of users, who have varying rights to view and modify resources")
    public static class UserGroup extends org.poscomp.fbps.model.UserGroup {

        private User creator ;

        @SuppressWarnings(value="unused")
        private UserGroup() {
            //needed by jackson
        }

        public UserGroup(org.poscomp.fbps.model.UserGroup group, org.poscomp.fbps.model.User creator) {

            super(group) ;

            if (creator != null)
                this.creator = new User(creator) ;
        }

        @ApiModelProperty(value="The user who created this group")
        public User getCreator() {
            return creator;
        }

    }

    @ApiModel(description="Assigns a user to a group, with a role")
    public static class GroupMember extends User {

        private String groupId ;

        private org.poscomp.fbps.model.GroupMember.Role role ;


        @SuppressWarnings(value="unused")
        private GroupMember() {
            //needed by jackson
        }

        public GroupMember(org.poscomp.fbps.model.GroupMember groupMember, org.poscomp.fbps.model.User user) {

            super(user) ;

            this.groupId = groupMember.getGroupId() ;
            this.role = groupMember.getRole() ;
        }

        @ApiModelProperty(value="The group this user belongs to", position=6)
        public String getGroupId() {
            return groupId;
        }

        @ApiModelProperty(value="The role this user user has within the group", position=7)
        public org.poscomp.fbps.model.GroupMember.Role getRole() {
            return role;
        }

    }

    @ApiModel(description="Requests a user to be given a group membership")
    public static class GroupMemberRequest {

        private ObjectId id ;
        private User user ;
        private UserGroup group ;
        private org.poscomp.fbps.model.GroupMember.Role role ;

        private org.poscomp.fbps.model.GroupMemberRequest.State state ;

        private Date requestedAt ;
        private Date resolvedAt ;
        private User resolvedBy ;



        @SuppressWarnings(value="unused")
        private GroupMemberRequest() {
            //needed by jackson
        }

        public GroupMemberRequest(
                org.poscomp.fbps.model.GroupMemberRequest request,
                org.poscomp.fbps.model.User user,
                org.poscomp.fbps.model.UserGroup group,
                org.poscomp.fbps.model.User groupCreator,
                org.poscomp.fbps.model.User resolvedBy
        ) {

            this.id = request.getId() ;
            this.user = new User(user) ;
            this.group = new UserGroup(group, groupCreator) ;
            this.role = request.getRole() ;
            this.state = request.getState() ;
            this.requestedAt = request.getRequestedAt() ;
            this.resolvedAt = request.getResolvedAt() ;

            if (resolvedBy != null)
                this.resolvedBy = new User(resolvedBy) ;
        }

        public ObjectId getId() {
            return id;
        }

        public User getUser() {
            return user;
        }

        public UserGroup getGroup() {
            return group;
        }

        public org.poscomp.fbps.model.GroupMember.Role getRole() {
            return role;
        }

        public org.poscomp.fbps.model.GroupMemberRequest.State getState() {
            return state;
        }

        public Date getRequestedAt() {
            return requestedAt;
        }

        public Date getResolvedAt() {
            return resolvedAt;
        }

        public User getResolvedBy() {
            return resolvedBy;
        }
    }



    @ApiModel(description="States whether a candidate group id is valid or not")
    public static class GroupIdCheck {

        public enum Result  {ok, invalid, taken} ;

        private Result result ;

        public GroupIdCheck(Result result) {
            this.result = result;
        }

        public Result getResult() {
            return result;
        }

    }



    public static class Invitation  {

        private ObjectId id;



        private UserGroup group ;
        private org.poscomp.fbps.model.GroupMember.Role role ;

        private org.poscomp.fbps.model.Invitation.State state ;

        private User createdBy ;
        private Date createdAt ;

        private User usedBy ;
        private Date usedAt ;


        public Invitation(org.poscomp.fbps.model.Invitation invitation,
                          org.poscomp.fbps.model.UserGroup group,
                          org.poscomp.fbps.model.User groupCreator,
                          org.poscomp.fbps.model.User createdBy,
                          org.poscomp.fbps.model.User usedBy) {

            this.id = invitation.getId() ;

            this.group = new UserGroup(group, groupCreator) ;
            this.role = invitation.getRole() ;

            this.state = invitation.getState() ;

            this.createdBy = new User(createdBy) ;
            this.createdAt = invitation.getCreatedAt() ;

            if (usedBy != null)
                this.usedBy = new User(usedBy) ;

            this.usedAt = invitation.getUsedAt() ;
        }

        public ObjectId getId() {
            return id;
        }

        public UserGroup getGroup() {
            return group;
        }

        public org.poscomp.fbps.model.GroupMember.Role getRole() {
            return role;
        }

        public org.poscomp.fbps.model.Invitation.State getState() {
            return state;
        }

        public User getCreatedBy() {
            return createdBy;
        }

        public Date getCreatedAt() {
            return createdAt;
        }

        public User getUsedBy() {
            return usedBy;
        }

        public Date getUsedAt() {
            return usedAt;
        }
    }

}
