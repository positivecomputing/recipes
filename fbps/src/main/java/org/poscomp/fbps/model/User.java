package org.poscomp.fbps.model;

import com.restfb.FacebookClient;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by dmilne on 5/06/2014.
 */
@Document
public class User {

    public static final User ANYONE = new User(FacebookProfile.ANYONE) ;
    public static final String ME = "me" ;

    @Id
    private String userId ;

    private FacebookProfile profile ;

    @Indexed
    private Date lastLogin ;

    @Indexed
    private String apiKey ;

    private String token ;

    private Date tokenExpiry ;

    @SuppressWarnings(value="unused")
    private User() {
        //required by jackson
    }

    public User(FacebookProfile profile) {

        this.userId = profile.getUserId() ;
        this.profile = profile ;
    }

    public String getUserId() {
        return userId ;
    }

    public FacebookProfile getProfile() {
        return profile ;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) { this.apiKey = apiKey; }

    public String getToken() {

       return token ;
    }

    public Date getTokenExpiry() {
        return tokenExpiry ;
    }

    public void updateProfile(FacebookProfile profile) {
        this.profile = profile ;
    }

    public void updateToken(FacebookClient.AccessToken accessToken) {
        this.token = accessToken.getAccessToken() ;
        this.tokenExpiry = accessToken.getExpires() ;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void registerLogin() {
        this.lastLogin = new Date() ;
    }
}
