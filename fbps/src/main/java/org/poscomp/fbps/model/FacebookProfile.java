package org.poscomp.fbps.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.restfb.json.JsonObject;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by dmilne on 12/06/2014.
 */

@Document
public class FacebookProfile {

    public static final String FIELDS = "id,name,email,bio,picture" ;

    public static FacebookProfile ANYONE = new FacebookProfile(GroupMember.ANY, "Anyone") ;

    @Id
    private String userId ;

    @JsonProperty
    private String name ;

    @JsonProperty
    private String email ;

    @JsonProperty
    private String picture ;

    @JsonProperty
    private String bio ;


    public FacebookProfile() {

    }

    public FacebookProfile(String userId, String name) {

        this.userId = userId ;
        this.name = name ;
    }

    public FacebookProfile(JsonObject rawProfile) {

        this.userId = rawProfile.getString("id") ;

        update(rawProfile) ;
    }

    public FacebookProfile(FacebookProfile profile) {

        this.userId = profile.getUserId() ;
        this.name = profile.getName() ;
        this.email = profile.getEmail() ;
        this.bio = profile.getBio() ;
        this.picture = profile.getPicture() ;
    }

    public void update(JsonObject rawProfile) {

        if (rawProfile.has("name"))
            this.name = rawProfile.getString("name") ;

        if (rawProfile.has("email"))
            this.email = rawProfile.getString("email") ;

        if (rawProfile.has("bio"))
            this.bio = rawProfile.getString("bio") ;

        if (rawProfile.has("picture")) {

            JsonObject picture = rawProfile.getJsonObject("picture");
            if (picture.has("data")) {
                JsonObject pictureData = picture.getJsonObject("data");

                if (pictureData.has("url"))
                    this.picture = pictureData.getString("url");
            }
        }
    }

    @ApiModelProperty(value="A unique id for the user", position=1)
    public String getUserId() {
        return userId;
    }

    @ApiModelProperty(value="The name of the user, as it is entered in Facebook", position=2)
    public String getName() {
        return name;
    }

    @ApiModelProperty(value="The email address of the user, if we were able to retrieve it from Facebook",position=3)
    public String getEmail() {
        return email;
    }

    @ApiModelProperty(value="A url to a small (50x50 px) profile image of this user", position=4)
    public String getPicture() {
        return picture;
    }

    @ApiModelProperty(value="A short description of the user, if we were able to retrieve it from Facebook", position=5)
    public String getBio() {
        return bio;
    }
}
