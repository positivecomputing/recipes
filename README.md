This repository contains several fairly light-weight example projects for you to crib from:

* [wefeel4j](https://bitbucket.org/positivecomputing/recipes/src/master/wefeel4j/) - A Java client for the [We Feel API](http://wefeel.csiro.au/#/api). It provides an example of what to do if you find yourself needing to consume an API, but cannot find a decent Java client for it. 
* [fitbit-client](https://bitbucket.org/positivecomputing/recipes/src/master/fitbit-client/) - A Java Web Application that retrieves private data stored by [FitBit](http://fitbit.com). It provides an example of how to authenticate using OAuth. 
* [simple-forum](https://bitbucket.org/positivecomputing/recipes/master/simple-forum/) - A Java + AngularJS Web application that provides a basic forum, with users, threads and posts. It provides an example of how to use [Spring MVC](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html) to provide a nice clean REST API that is automatically documented using [Swagger](https://helloreverb.com/developers/swagger). It also shows how to put a pretty face on that API using [AngularJS](https://angularjs.org/) and [Bootstrap](http://getbootstrap.com/). 
* [facebook-forum](https://bitbucket.org/positivecomputing/recipes/src/master/facebook-forum/) - An extension to *simple-forum* that provides "Sign in with Facebook" functionality to authenticate users. 


This last project relies on:

* [fbps](https://bitbucket.org/positivecomputing/recipes/src/master/fbps/) - Short for Facebook Permission System, this project provides the basic framework (users, groups, roles, etc) to develop apps that use Facebook accounts to identify and authenticate users. It is not expected that you would crib from or edit this, but you might want to incorporate it into your own projects in the same way that *facebook-forum* does.